import React from 'react'
import { Box, useColorModeValue, Heading, Input } from '@chakra-ui/react'
import { bgModal } from '@styles/variables'

interface CouponProps {
  sumTotal?: () => void
  valueProduct: { discount: number; total: number; subTotal: number }
  active?: boolean
}
type HeadingCType = {
  label: string
  type?: string
}

const Coupon: React.FC<CouponProps> = ({ sumTotal, valueProduct, active }) => {
  const bg = useColorModeValue(bgModal.light, bgModal.dark)
  const HeadingC = ({ label, type }: HeadingCType) => (
    <Heading as="h5" fontSize={type === 'total' ? '20px' : '16px'} fontWeight="600" mt="0.7em">
      <label htmlFor={label}> {label}</label>
    </Heading>
  )
  return (
    <Box bg={bg} borderRadius="1em" boxShadow="2px 2px 2px rgba(0,0,0,0.2)" p="1rem">
      <Box>
        <Box d="flex" px="1rem" justifyContent="space-between">
          <HeadingC label="Tax" />
          <HeadingC label="10" />
        </Box>
        <Box d="flex" px="1rem" justifyContent="space-between">
          <HeadingC label="Subtotal" />
          <HeadingC label={`${valueProduct.subTotal}`} />
        </Box>
        {active && sumTotal && (
          <>
            <Box d="flex" px="1rem" justifyContent="space-between">
              <HeadingC label="Descuento" />
              <HeadingC label={`${valueProduct.discount}%`} />
            </Box>
            <Box d="flex" px="1rem" justifyContent="space-between" mt="1rem">
              <HeadingC label="Cupon" />
              <Box d="flex" justifyContent="flex-end">
                <Input mx="0.5em" w="50%" onChange={e => sumTotal(e)} />
                <HeadingC label={`${valueProduct.discount}%`} />
              </Box>
            </Box>
            {/* <Heading>
              <label htmlFor="Cupon">Cupon:</label>
              <CouponInput type="text" />
            </Heading> */}
          </>
        )}
        <Box d="flex" px="1rem" justifyContent="space-between">
          <HeadingC label="TOTAL:" type="total" />
          <HeadingC type="total" label={`${valueProduct.total}`} />
        </Box>
      </Box>
    </Box>
  )
}

export default Coupon
