import React from 'react'
import Link from 'next/link'
import { Img } from '@chakra-ui/react'
import {
  Switch,
  Box,
  useColorMode,
  FormControl,
  FormLabel,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  Image
} from '@chakra-ui/react'
import { FaMoon, FaSun } from 'react-icons/fa'
import { useRouter } from 'next/router'
import { string } from 'yup/lib/locale'

export const locationSelect = {
  'es-MX': '/img/mexico.svg',
  'es-CO': '/img/colombia.svg',
  'en-US': '/img/usa.svg'
}

const Header: React.FC = () => {
  const { colorMode, toggleColorMode } = useColorMode()
  const { locale, push, asPath, pathname, query } = useRouter()

  const setFlag = (location: { location: string }) => {
    push({ pathname, query }, asPath, { locale: location })
  }

  return (
    <Box w="100%" height="100%" px="1rem" d="flex" justifyContent="space-between" alignItems="center">
      <Box>
        <FormControl display="flex">
          <Switch display="none" id="OWO" onChange={toggleColorMode} />
          <FormLabel fontSize="22px" cursor="pointer" ml="1rem" htmlFor="OWO">
            {colorMode === 'light' ? <FaMoon /> : <FaSun />}
          </FormLabel>
        </FormControl>
      </Box>
      <Link href="/">
        <Img src="/img/logo.png" alt="Logo" cursor="pointer" boxSize="50px" backgroundPosition="cover" />
      </Link>
      <Menu>
        <MenuButton aria-label="select-location" mr="1rem">
          <Image boxSize="30px" src={locationSelect[locale]} alt={locale} w="44px" />
        </MenuButton>
        <MenuList>
          <MenuItem aria-label="es-MX" onClick={() => setFlag('es-MX')}>
            MX
          </MenuItem>
          <MenuItem aria-label="es-CO" onClick={() => setFlag('es-CO')}>
            CO
          </MenuItem>
          <MenuItem aria-label="en-US" onClick={() => setFlag('en-US')}>
            US
          </MenuItem>
        </MenuList>
      </Menu>
    </Box>
  )
}

export default Header
