import {
  Box,
  Button,
  IconButton,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure
} from '@chakra-ui/react'
import React from 'react'
import 'react-confirm-alert/src/react-confirm-alert.css'
import { HiTrash } from 'react-icons/hi'

interface AlertProps {
  handleRemoveFromCart: () => void
}

const Alert: React.FC<AlertProps> = ({ handleRemoveFromCart }) => {
  const { isOpen, onOpen, onClose } = useDisclosure()
  const handleClose = () => {
    onClose()
    handleRemoveFromCart()
  }
  return (
    <Box>
      <IconButton
        size="sm"
        bg="primary.100"
        borderRadius="1.5em"
        aria-label="Delete item"
        _hover={{
          color: 'primary.900',
          border: '1px solid primary.900',
          backgroundColor: 'primary.500',
          boxShadow:
            '0 4px 4px rgba(0, 0, 0, .25), inset 0 4px 3px rgba(255, 255, 255, .65), inset 0 -4px 2px primary.900'
        }}
        _active={{
          color: 'blancos.100',
          border: '1px solid primary.200',
          backgroundColor: 'primary.100',
          boxShadow:
            '0 4px 4px rgba(0, 0, 0, .25), inset 0 4px 3px rgba(255, 255, 255, .65), inset 0 -4px 2px primary.200'
        }}
        icon={<HiTrash color="#fff" size="16px" />}
        onClick={onOpen}
      />
      <Modal isOpen={isOpen} onClose={onClose} isCentered>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Confirmar para eliminar</ModalHeader>
          <ModalCloseButton />
          <ModalBody>¿Estás seguro de hacer esto?</ModalBody>

          <ModalFooter>
            <Button aria-label="no" colorScheme="blue" mr={3} onClick={onClose}>
              No
            </Button>
            <Button aria-label="si" variant="ghost" onClick={handleClose}>
              Si
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Box>
  )
}

export default Alert
