import React from 'react'
import { CardProp } from 'types/app'
import { GoPlus } from 'react-icons/go'
import { Box, Image, IconButton, useColorModeValue, Text, Input } from '@chakra-ui/react'
import Alert from './Alert'
import { bgModal } from '../../styles/variables'

const Card: React.FC<CardProp> = ({ item, handleRemoveFromCart, handleSubtractQuantity, handleAddQuantity }) => {
  const bg = useColorModeValue(bgModal.light, bgModal.dark)
  return (
    <Box
      alignItems="center"
      bg={bg}
      d="flex"
      w="100%"
      my="1rem"
      py="1rem"
      pr="1rem"
      justifyContent="space-between"
      borderRadius="0.5em"
    >
      <Box>
        <Image src={item.image} w="100%" />
      </Box>
      <Box>
        <Box p="6px" d="flex">
          <Box w="100%">
            <Text fontSize="14px" fontWeight="600">
              {item.name}
            </Text>
            <Text fontSize="14px" fontWeight="500">
              {item.price}
            </Text>
          </Box>
          <Alert handleRemoveFromCart={handleRemoveFromCart} />
        </Box>
        <Box d="flex" mt="0.5em">
          <IconButton aria-label="add more" onClick={handleAddQuantity} icon={<GoPlus size="16px" />} />
          <Input value={item.quantity} mx="0.5em" />
          <IconButton
            aria-label="add more"
            onClick={handleSubtractQuantity}
            icon={
              <Box d="flex" alignItems="center" fontSize="32px" fontWeight="600" mb="8px">
                -
              </Box>
            }
            disabled={item.quantity && item.quantity > 1 ? false : false}
          />
        </Box>
      </Box>
    </Box>
  )
}

export default Card
