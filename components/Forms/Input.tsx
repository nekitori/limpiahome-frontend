import React from 'react'
import { Field, ErrorMessage } from 'formik'
import { InputProps } from './props'
import { Box } from '@chakra-ui/react'

const Input: React.FC<InputProps> = ({ name, placeholder, type = 'text', as = 'input', setFieldValue }) => {
  return (
    <Box position="relative" pb="1.3rem" mb="0.6rem">
      <label htmlFor="title">{placeholder}</label>
      {type === 'file' ? (
        <input
          style={{ backgroundColor: 'transparent' }}
          name={name}
          type="file"
          onChange={(e: React.FormEvent<HTMLInputElement>) => {
            setFieldValue(name, e.currentTarget.files[0])
          }}
        />
      ) : (
        <Field
          style={{ backgroundColor: 'transparent', marginLeft: '10px' }}
          as={as}
          name={name}
          type={type}
          placeholder={placeholder}
        />
      )}
      <Box position="absolute" bottom="0" color="crimson">
        <ErrorMessage name={name} component="span" />
      </Box>
    </Box>
  )
}

export default Input
