import React from 'react'
import Select from 'react-select'
import { SelectListProps } from './props'

const customThemeFn = theme => ({
  ...theme,
  spacing: {
    ...theme.spacing,
    controlHeight: 30,
    baseUnit: 2
  }
})

const SelectList: React.FC<SelectListProps> = ({ name, options, setFieldValue, placeholder }) => {
  return (
    <>
      <span>{placeholder}</span>
      <Select theme={customThemeFn} isMulti options={options} onChange={e => setFieldValue(name, e)}></Select>
    </>
  )
}

export default SelectList
