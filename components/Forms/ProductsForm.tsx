import { FormContainer } from './styles'
import { useAppContext } from 'hooks/useAppContext'
import { ProductsFormProps } from 'types/app'
import { Form, Formik } from 'formik'
import ProductSchema from 'schemas/ProductSchema'
import Input from './Input'
import SelectList from './SelectList'

const initialValues = {
  name: '',
  description: '',
  presentation: '',
  price: '',
  categories: '',
  image: ''
}

const ProductsForm: React.FC<ProductsFormProps> = ({ editId }) => {
  const {
    state: { categories, products }
  } = useAppContext()

  const options = categories.map(category => ({ value: category.id, label: category.name }))

  let defaultData

  if (editId) {
    defaultData = products.find(product => product.id === editId)
  }

  const handleSubmit = e => {
    console.log(e)
    /**
     * TODO:
     * send request to backend
     **/
  }

  return (
    <Formik initialValues={defaultData || initialValues} validationSchema={ProductSchema} onSubmit={handleSubmit}>
      {({ isSubmitting, setFieldValue, handleSubmit }) => (
        <Form style={{ display: 'block', width: '100%' }} onSubmit={handleSubmit}>
          <Input name="name" placeholder="Name" />
          <Input name="description" placeholder="Description" as="textarea" />
          <Input name="presentation" placeholder="Presentation" />
          <Input name="price" placeholder="Price" type="number" />
          <SelectList name="categories" options={options} setFieldValue={setFieldValue} />
          <Input name="image" placeholder="Image" type="file" setFieldValue={setFieldValue} />
          <button aria-label="submit-products" type="submit" disabled={isSubmitting}>
            Submit
          </button>
        </Form>
      )}
    </Formik>
  )
}

export default ProductsForm
