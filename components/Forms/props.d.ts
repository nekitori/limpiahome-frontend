export interface InputProps {
  name: string
  type?: string
  as?: string
  placeholder?: string
  setFieldValue: (name: string, file: any | null) => void | null
}

export interface SelectListProps {
  name: string
  placeholder?: string
  option: { value: string; label: string }
  setFieldValue: (name: string, file: any | null) => void | null
}
