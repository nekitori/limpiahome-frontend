import styled from 'styled-components'
import { Form, ErrorMessage } from 'formik'

export const FormContainer = styled(Form)`
  input,
  textarea {
    display: block;
    width: 100%;
  }
`

export const InputContainer = styled.div`
  position: relative;
  padding-bottom: 1.3rem;
  margin-bottom: 0.6rem;
`

export const ErrorMsg = styled(ErrorMessage)`
  position: absolute;
  bottom: 0;
  color: #c40000;
`
