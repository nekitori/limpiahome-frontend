import { useAppContext } from 'hooks/useAppContext'
import { ProductsFormProps } from 'types/app'
import { Formik, Form } from 'formik'
import ProductSchema from 'schemas/ProductSchema'
import Input from './Input'
import { useState } from 'react'

const CategoriesForm: React.FC<ProductsFormProps> = ({ editId }) => {
  const { state } = useAppContext()
  const categories = state?.categories
  const [defaultData, setDefaultData] = useState<string | undefined>('')

  if (editId) {
    const theName = categories?.find(category => category.id === editId)?.name
    setDefaultData(theName)
  }

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
  }

  return (
    <Formik initialValues={defaultData} validationSchema={ProductSchema} onSubmit={handleSubmit}>
      {({ isSubmitting }) => (
        <Form>
          <Input name="name" placeholder="Name" setFieldValue={defaultData} />
          <button aria-label="submit-categorie" type="submit" disabled={isSubmitting}>
            Submit
          </button>
        </Form>
      )}
    </Formik>
  )
}

export default CategoriesForm
