import React, { useState } from 'react'
import ProfilePhoto from 'components/Icons/ProfilePhoto'
import SettingsIcon from 'components/Icons/Settings'
import EditText from '@components/Atoms/EditText'
import { Formik } from 'formik'
import { Button, Text, Box, Heading } from '@chakra-ui/react'

const initValue = {
  dni: 'lorem ipsum dolor',
  address: 'lorem ipsum dolor',
  subscription: 'lorem ipsum dolor',
  country: 'lorem ipsum dolor'
}

const Information: React.FC = () => {
  const [edit, setEdit] = useState(false)

  return (
    <>
      <Formik initialValues={initValue} onSubmit={e => console.log(e)}>
        {({ handleSubmit }) => (
          <Box>
            <form action="" onSubmit={handleSubmit} style={{ backgroundColor: 'transparent' }}>
              <Box textAlign="center">
                <ProfilePhoto />
              </Box>
              <Heading as="h2" textAlign="center">
                User Name
              </Heading>
              <Text my="0.5rem">
                <Text fontWeight="bold" fontSize="17px">
                  DNI
                </Text>
                <EditText name="dni" onEdit={edit}></EditText>
              </Text>
              <Text my="0.5rem">
                <Text fontWeight="bold" fontSize="17px">
                  Deslivery Address
                </Text>
                <EditText name="address" onEdit={edit}></EditText>
              </Text>
              <Text my="0.5rem">
                <Text fontWeight="bold" fontSize="17px">
                  Country
                </Text>
                <EditText name="country" onEdit={edit}></EditText>
              </Text>
              <Text my="0.5rem">
                <Text fontWeight="bold" fontSize="17px">
                  Subscription
                </Text>
                <EditText name="subscription" onEdit={edit}></EditText>
              </Text>
            </form>
          </Box>
        )}
      </Formik>
      <Button
        border="1px solid #9da6a5"
        onClick={() => setEdit(!edit)}
        boxShadow="0px 2px 2px rgba(0, 0, 0, 0.25), inset 0px -3px 2px #9da6a5, inset 0px 3px 2px rgba(255, 255, 255, 0.65)"
        borderRadius="10px"
        p="0.5rem"
      >
        <SettingsIcon />
      </Button>
    </>
  )
}

export default Information
