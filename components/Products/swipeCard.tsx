import React, { ReactElement, useState } from 'react'
import { Box } from '@chakra-ui/react'
import { useSprings, animated, interpolate } from 'react-spring'
import { useDrag } from 'react-use-gesture'
import ProductCard from './ProductCard'

const to = i => ({ x: 0, y: 0, scale: 1, delay: 200 })
const from = i => ({ x: 0, rot: 0, scale: 1.2, y: -200 })

const trans = (r, s) => `rotateY(${r / 10}deg) scale(${s})`

// Products.id

export default function SwipeCard({ productsId, addToCart }): ReactElement {
  const [gone] = useState(() => new Set()) // The set flags all the cards that are flicked out
  const [props, set] = useSprings(productsId.length, i => ({ ...to(i), from: from(i) })) // Create a bunch of springs using the helpers above
  // Create a gesture, we're interested in down-state, delta (current-pos - click-pos), direction and velocity
  const bind = useDrag(({ args: [index], down, movement: [mx], direction: [xDir], velocity }) => {
    const trigger = velocity > 0.2 // If you flick hard enough it should trigger the card to fly out
    const dir = xDir < 0 ? -1 : 1 // Direction should either point left or right
    if (!down && trigger) {
      gone.add(index)
      addToCart(productsId[index], dir === 1 ? 'right' : 'left')
    } // If button/finger's up and trigger velocity is reached, we flag the card ready to fly out

    set(i => {
      if (index !== i) return // We're only interested in changing spring-data for the current spring
      const isGone = gone.has(index)
      const x = isGone ? (200 + window.innerWidth) * dir : down ? mx : 0 // When a card is gone it flys out left or right, otherwise goes back to zero
      const rot = mx / 100 + (isGone ? dir * 10 * velocity : 0) // How much the card tilts, flicking it harder makes it rotate faster
      const scale = down ? 1.1 : 1 // Active cards lift up a bit
      return { x, rot, scale, delay: undefined, config: { friction: 50, tension: down ? 800 : isGone ? 200 : 500 } }
    })

    /*re aparecer los elementos*/
    if (!down && gone.size === productsId.length) setTimeout(() => gone.clear() || set(i => to(i)), 600)
  })
  return (
    <Box d="flex" position="relative" alignItems="center" w="100%" mt="1em">
      {props.map(({ x, y, rot, scale }, i) => (
        <animated.div
          key={i}
          style={{
            transform: interpolate([x, y], (x, y) => `translate3d(${x}px,${y}px,0)`),
            position: 'absolute',
            top: 0,
            width: '100%',
            willChange: 'transform',
            display: 'flex',
            overflow: 'hidden',
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <animated.div
            {...bind(i)}
            style={{
              transform: interpolate([rot, scale], trans),
              backgroundColor: 'white',
              backgroundSize: 'cover',
              backgroundRepeat: 'none',
              width: '100%',
              aspectRatio: '1',
              willChange: 'transform',
              borderRadius: '10px'
            }}
          >
            <ProductCard id={productsId[i]} />
          </animated.div>
        </animated.div>
      ))}
    </Box>
  )
}
