import React from 'react'
import { FaStar } from 'react-icons/fa'
import { Box, Heading, useColorModeValue, Image } from '@chakra-ui/react'
import { bgModal } from '../../styles/variables'
function Carousel() {
  const bg = useColorModeValue(bgModal.light, bgModal.dark)
  return (
    <Box bg={bg} p="1rem" borderRadius="1em 1em 0 0">
      <Heading as="h1" fontSize="16px">
        title
      </Heading>
      <Image src="https://arepa.s3.amazonaws.com/camiseta.png" />
      <Box d="flex" fontSize="22px" justifyContent="space-between" p="1rem">
        <FaStar />
        <FaStar />
        <FaStar />
        <FaStar />
        <FaStar />
      </Box>
    </Box>
  )
}

export default Carousel
