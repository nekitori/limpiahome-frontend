import styled from 'styled-components'

export const MainDetails = styled.main`
  position: absolute;
  padding: 1rem;
  top: 0;
  left: 0;
  background-color: blueviolet;
  display: grid;
  grid-template-rows: 400px 195px 140px 300px;
`
export const Wrapper = styled.div`
  background-color: orange;
  position: relative;
  .title,
  .rate,
  .left,
  .right,
  .picture {
    position: absolute;
    z-index: 5;
  }
  .title,
  .rate,
  .left,
  .right {
    z-index: 10;
  }
  .title {
    background-color: whitesmoke;
    width: 100%;
    height: 50px;
    text-align: center;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 40px;
  }
  .rate {
    bottom: 1rem;
    width: 100%;
    height: 2rem;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: white;
    div {
      width: 60%;
      display: flex;
      justify-content: space-between;
      font-size: 10px;
    }
  }
  .left {
    left: 1rem;
    border-radius: 50%;
    background-color: white;
    width: 40px;
    height: 40px;
    top: calc(50% - 20px);
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
  }
  .right {
    right: 1rem;
    border-radius: 50%;
    background-color: white;
    width: 40px;
    height: 40px;
    top: calc(50% - 20px);
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
  }
  .picture {
    background-image: url('https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmedia1.popsugar-assets.com%2Ffiles%2Fthumbor%2FjlOFWPqUEEUD-fjDZ-8FVs50uZY%2Ffit-in%2F1024x1024%2Ffilters%3Aformat_auto-!!-%3Astrip_icc-!!-%2F2018%2F07%2F11%2F968%2Fn%2F1922441%2F61e2ac515b46812781bec4.42266919_%2Fi%2FEco-Friendly-Cleaning-Products.jpg&f=1&nofb=1');
    width: 100%;
    height: 100%;
    background-position: cover;
  }
`

export const Description = styled.p`
  background-color: brown;
  padding: 2rem 1rem 1rem;
  font-size: 14px;
  line-height: 18px;
  text-align: center;
`
export const ButtonCA = styled.button`
  background-color: lime;
  justify-self: center;
  align-self: center;
  width: 200px;
  height: 50px;
  border-radius: 5px;
  border: none;
  cursor: pointer;
`
export const Box = styled.section`
  background-color: yellowgreen;
  display: grid;
  grid-template-rows: 1fr 50px;
  .up {
    position: relative;
    overflow-x: hidden;
    overflow-y: scroll;
  }
  .base {
    position: absolute;
    background-color: blue;
    padding: 0.5rem;
    width: 100%;
    height: 200%;
  }
  .card-comment {
    background-color: whitesmoke;
    padding: 10px;
    display: grid;
    grid-template-rows: 60px 1fr;
  }
  .avatar {
    justify-self: center;
    align-self: center;
    background-color: brown;
    border-radius: 50%;
    width: 50px;
    height: 50px;
  }
  .comment {
    font-size: 14px;
    line-height: 18px;
    text-align: center;
  }
  .down {
    display: flex;
    justify-content: center;
    align-items: center;
    input {
      margin-right: 10px;
    }
  }
`

export const ButtonsBox = styled.div`
  background-color: brown;
  display: flex;
  justify-content: space-evenly;
  div {
    background-color: blue;
    display: block;
    font-weight: 400;
    justify-content: center;
    align-items: center;
    width: 100%;
    font-size: 12px;
    text-align: center;
  }
  button {
    border-radius: 50%;
    width: 5rem;
    height: 5rem;
  }
`
export const CateBox = styled.div`
  background-color: pink;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 2rem;
`
export const Card = styled.div`
  background-color: lime;
  position: relative;
  height: 100%;
  width: 100%;
  .wrapper {
    padding: 20px 10px;
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
  }
  .card {
    background-image: url('https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.picturecorrect.com%2Fwp-content%2Fuploads%2F2012%2F07%2Fproduct-photography.jpg&f=1&nofb=1');
    background-position: cover;
    display: grid;
    grid-template-rows: 1fr 30%;
    width: 100%;
    height: 400px;
  }

  .card-body {
    background-color: pink;
    padding: 0 10px;
    display: grid;
    grid-template-rows: 2rem 1fr;
  }
  .card-body__header {
    background-color: green;
    display: grid;
    grid-template-columns: 1fr 50px 50px;
    .title,
    .price,
    .rate {
      align-self: center;
    }
    .price,
    .rate {
      justify-self: center;
    }
    .title {
      background-color: yellowgreen;
      width: 100%;
    }
    .price {
      background-color: blanchedalmond;
    }
    .rate {
      background-color: pink;
    }
  }
  .card-body-description {
    background-color: cadetblue;
  }
`
export const Main = styled.main`
  background-color: yellow;
  padding: 1rem 1rem;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  display: grid;
  grid-template-rows: 2rem 1fr 5rem;
  position: absolute;
`
