import React from 'react'
import { Box, Select, useColorModeValue } from '@chakra-ui/react'
import { CateBox } from './styles'
import { bgInput } from '../../styles/variables'
import { useGetState } from 'hooks/useGetState'

function Category({ onChange }) {
  const categories = useGetState('categories')
  const bg = useColorModeValue(bgInput.light, bgInput.dark)
  return (
    <Box>
      <Select bg={bg} placeholder="Select Your Category" onChange={onChange}>
        {categories.map(category => (
          <option key={category.id} value={category.id}>
            {category.name}
          </option>
        ))}
      </Select>
    </Box>
  )
}

export default Category
