import React from 'react'
import { Box, useColorModeValue, Text } from '@chakra-ui/react'
import { FaUserAlt } from 'react-icons/fa'
import { bgModal } from '../../styles/variables'

function CommentBox(): JSX.Element {
  const bg = useColorModeValue(bgModal.light, bgModal.dark)
  return (
    <Box bg={bg} p="1rem" borderRadius="1em" mt="1em">
      <Box d="flex" fontSize="16px" alignItems="center">
        <Box w="30%">
          <FaUserAlt size="60px" />
        </Box>
        <Text w="70%">
          "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium. Sed ut
          perspiciatis unde omnis iste
        </Text>
      </Box>
    </Box>
  )
}

export default CommentBox
