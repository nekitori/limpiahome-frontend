import React, { ReactElement, ReactNode } from 'react'
import { Box } from '@chakra-ui/react'

interface Props {
  children: ReactNode
}

export default function Main({ children }: Props): ReactElement {
  return (
    <Box
      d="flex"
      flexDirection="column"
      justifyContent="flex-start"
      overflow="hidden"
      h="calc(100vw + 3rem)"
      pos="relative"
    >
      {children}
    </Box>
  )
}
