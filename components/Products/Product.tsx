import React from 'react'
import { Main } from './styles'

const Product = ({ productItem, handleAddToCart }) => {
  return (
    <Main>
      <h3>{productItem.name}</h3>
      <button aria-label="add-to-cart" onClick={() => handleAddToCart(productItem)}>
        Add to cart
      </button>
    </Main>
  )
}

export default Product
