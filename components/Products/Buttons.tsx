import Link from 'next/link'
import { FiShoppingCart } from 'react-icons/fi'
import { Box, useColorModeValue, Heading } from '@chakra-ui/react'
import React from 'react'
import { bgModal, colorInput } from '../../styles/variables'

function Buttons({ lastDirection, cartItems }): JSX.Element {
  const bg = useColorModeValue(bgModal.light, bgModal.dark)
  const color = useColorModeValue(colorInput.light, colorInput.dark)
  return (
    <>
      <Box bg={bg} color={color} mt="1em" textAlign="center" borderRadius="1em">
        <Box color={color} padding="1rem" textAlign="center">
          {lastDirection === 'right' ? (
            <Heading as="h2" fontWeight="500" fontSize="22x">
              Product Added
            </Heading>
          ) : lastDirection === 'left' ? (
            <Heading as="h2" fontWeight="500" fontSize="22x">
              Product Discarded
            </Heading>
          ) : (
            <Heading as="h2" fontWeight="500" fontSize="22x">
              Swipe a card or press a button to get started!
            </Heading>
          )}
          {cartItems ? (
            <Heading as="h2" fontWeight="500" fontSize="22x">
              {' '}
              Items in your cart {cartItems}{' '}
            </Heading>
          ) : (
            ''
          )}
          <Box mt="1em">
            <Link href="/checklist">
              <FiShoppingCart size="22px" style={{ margin: '0 auto' }} />
            </Link>
          </Box>
        </Box>
      </Box>
    </>
  )
}

export default Buttons
