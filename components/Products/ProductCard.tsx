import React from 'react'
import { Box, Image, Text } from '@chakra-ui/react'
import { useAppContext } from 'hooks/useAppContext'
import { useRouter, userRouter } from 'next/router'

const ProductCard = ({ id }) => {
  const { state } = useAppContext()
  const { push } = useRouter()
  const product = state.products.find(product => product.id === id) || {}

  console.log('rendered')

  return (
    <Box position="relative" h="100%">
      <Image
        src="https://metrocolombiafood.vteximg.com.br/arquivos/ids/274186-1000-1000/7702010225123.jpg?v=637281993884100000"
        objectFit="cover"
        w="100%"
        alt="metrocolombia"
        h="100%"
        borderRadius="10px"
      />
      <Box
        position="absolute"
        top="0"
        left="0"
        p="1rem"
        w="100%"
        h="100%"
        background="linear-gradient(0deg, rgba(0, 0, 0,.9 ) 0%, rgba(3, 50, 89,.1) 80%);"
        borderRadius="8px"
      >
        <Text as="h2" fontSize="2xl" fontWeight="700" color="#000">
          {product.name}
        </Text>
        <Box
          position="absolute"
          left="0"
          w="100%"
          bottom="0"
          p="1rem"
          display="flex"
          justifyContent="space-between"
          alignItems="flex-end"
          onClick={() => push('/products/details')}
        >
          <Text
            color="#fff"
            overflow="hidden"
            textOverflow="ellipsis"
            maxH="2.4em"
            lineHeight="1.2em"
            marginRight="1rem"
          >
            {product.description}
          </Text>
          <Text color="#fff" as="h2" fontSize="2xl" fontWeight="700">
            ${product.price}
          </Text>
        </Box>
      </Box>
    </Box>
  )
}

export default ProductCard
