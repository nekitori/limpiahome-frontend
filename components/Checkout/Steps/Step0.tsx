import React, { ReactElement } from 'react'
import Button from '@components/Shared/Button'
import { Box } from '@chakra-ui/react'

interface Props {
  handleChange: () => void
  back: () => void
}

export default function Step0({ handleChange, back }: Props): ReactElement {
  return (
    <Box d="flex" flexDirection="column" justifyContent="space-around" alignItems="center" h="40vh" w="90%" m="0 auto">
      <Button label="Paypal" width="100%" type="primary" />
      <Button label="Stripe" width="100%" type="primary" />
      <Button label="Continue" width="100%" type="secondary" onClick={() => handleChange()} />
      <Button type="secondary" width="100%" label="Back" onClick={back} />
    </Box>
  )
}
