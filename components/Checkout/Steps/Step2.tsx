import React, { ReactElement, useState } from 'react'
import { Box } from '@chakra-ui/react'
import Button from '@components/Shared/Button'
import CardAddress from '@components/Shared/CardAddress'
import Coupon from '@components/Coupon'

interface Props {
  handleChange: () => void
  back: () => void
}
interface ValueProps {
  discount: number
  total: number
  subTotal: number
}

export default function Step2({ handleChange, back }: Props): ReactElement {
  const [valueProduct, setValueProduct] = useState<ValueProps>({ discount: 0, total: 0, subTotal: 0 })

  return (
    <Box mt="1em">
      <CardAddress />
      <Box my="1.5em">
        <Coupon valueProduct={valueProduct} active={true} sumTotal={() => {}} />
      </Box>
      <Button type="primary" width="100%" label="Confirm" onClick={handleChange} />
      <Box my="1em">
        <Button type="secondary" width="100%" label="Back" onClick={back} />
      </Box>
    </Box>
  )
}
