import React, { ReactElement, useState } from 'react'
import {
  Box,
  Button,
  Heading,
  useColorModeValue,
  Menu,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  MenuButton,
  MenuList,
  MenuItem,
  Input,
  useDisclosure
} from '@chakra-ui/react'
import Boton from '@components/Shared/Button'
import { AiFillCaretDown } from 'react-icons/ai'
import CardAddress from '@components/Shared/CardAddress'
import { bgInput } from '@styles/variables'
interface Props {
  handleChange: () => void
}

type AddressType = {
  title: string
  address?: any
  type: string
}

type MappingOp = {
  label: string
  type: string
}

type AddressData = {
  id: number
  name?: string
  billing: boolean
  country?: string
  city?: string
  zipCode?: string
  street?: string
  externalNumber?: string
  fullName?: string
  phone?: string
}

const initData = {
  billing: [
    {
      id: 2,
      name: 'Casa de mis padres',
      billing: true,
      fullName: 'keanu reeves',
      phone: '5555555555',
      country: 'USA',
      city: 'los angeles',
      zipCode: '09220',
      street: 'street',
      externalNumber: 666,
      internalNumber: 8,
      note: 'Cuidado con el perro',
      userId: 1
    },
    {
      id: 2,
      name: 'Casa de mis padres',
      billing: true,
      fullName: 'keanu reeves',
      phone: '5555555555',
      country: 'USA',
      city: 'los angeles',
      zipCode: '09220',
      street: 'street',
      externalNumber: 666,
      internalNumber: 8,
      note: 'Cuidado con el perro',
      userId: 1
    },
    {
      id: 2,
      name: 'Casa de mis padres',
      billing: true,
      fullName: 'keanu reeves',
      phone: '5555555555',
      country: 'USA',
      city: 'los angeles',
      zipCode: '09220',
      street: 'street',
      externalNumber: 666,
      internalNumber: 8,
      note: 'Cuidado con el perro',
      userId: 1
    }
  ],
  delivery: [
    {
      id: 3,
      name: 'Mi casa',
      billing: false,
      fullName: 'hijo de keanu reeves',
      phone: '5555555554',
      country: 'USA',
      city: 'los angeles',
      zipCode: '09270',
      street: 'super street',
      externalNumber: 8,
      internalNumber: 1,
      note: 'Toca 5 veces',
      userId: 1
    }
  ]
}

export default function Step1({ handleChange }: Props): ReactElement {
  const bg = useColorModeValue(bgInput.light, bgInput.dark)
  const [newAddress, setNewAddress] = useState('none')
  const [showAddressB, setShowAddressB] = useState(false)
  const [showAddressD, setShowAddressD] = useState(false)

  const [dataBilling, setDataBilling] = useState(initData.billing[0])
  const [dataDelivery, setDataDelivery] = useState(initData.delivery[0])

  const { isOpen, onOpen, onClose } = useDisclosure()

  const handleShow = (type: boolean) => {
    type ? setShowAddressB(true) : setShowAddressD(true)
  }

  const mappingOptions = (data: AddressData, i: number) => (
    <MenuItem w="100%" aria-label={data.name} key={i} onClick={() => handleShow(data.billing)}>
      {data.name}
    </MenuItem>
  )
  const ModalC = () => (
    <Modal isOpen={isOpen} onClose={onClose} isCentered>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Insert New Address: {newAddress}</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <Input placeholder="Full Name" my="0.5em" />
          <Input placeholder="Address" my="0.5em" />
          <Input placeholder="Country" my="0.5em" />
          <Input placeholder="City" my="0.5em" />
          <Input placeholder="Phone" my="0.5em" />
          <Input placeholder="Zip Code" my="0.5em" />
          <Input placeholder="Phone" my="0.5em" />
          <Input placeholder="Zip Code" my="0.5em" />
        </ModalBody>
        <ModalFooter>
          <Button aria-label="close" colorScheme="blue" mr={3} onClick={onClose}>
            Close
          </Button>
          <Button aria-label="confirm step1" variant="ghost" onClick={() => {}}>
            Confirm
          </Button>
          <Button aria-label="confirm" variant="ghost">
            Confirm
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  )
  const Address = ({ title, address, type }: AddressType) => (
    <Box mt="1rem">
      <Box d="flex" justifyContent="space-between" alignItems="center" py="1rem">
        <Heading as="h3" fontSize="22px" my="0.5em" fontWeight="400">
          {title}
        </Heading>
        {title === 'Delivery Address' && (
          <Button aria-label="same address" borderRadius="1em">
            Same Address
          </Button>
        )}
      </Box>
      <Menu>
        <MenuButton
          bg={bg}
          w="100%"
          aria-label="select one address"
          as={Button}
          rightIcon={<AiFillCaretDown />}
          onClick={() => console.log('pwpp')}
        >
          Select one address
        </MenuButton>
        <MenuList w="100%">
          {address && address.map(mappingOptions)}
          <MenuItem
            w={{ base: '91vw', md: '58vw', lg: '32.2vw' }}
            color="primary.500"
            aria-label="agregar address"
            onClick={() => {
              setNewAddress(type)
              onOpen()
            }}
          >
            Agregar
          </MenuItem>
        </MenuList>
      </Menu>
    </Box>
  )
  return (
    <Box>
      <Heading as="h2" fontSize="20px" fontWeight="400">
        Select and confirm your delivery info:
      </Heading>
      <Address type="Billing" title="Billing Address" address={initData.billing} />
      <Box my="1em">{showAddressB && <CardAddress data={dataBilling} />}</Box>
      <Address type="Delivery" title="Delivery Address" address={initData.delivery} />
      <Box my="1em">{showAddressD && <CardAddress data={dataDelivery} />}</Box>

      <Box my="1em">
        <Boton type="primary" label="Confirm" width="100%" onClick={handleChange} />
        <ModalC />
      </Box>
    </Box>
  )
}
