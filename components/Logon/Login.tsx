import theInput from '@components/Atoms/Input'
import { Box, Button } from '@chakra-ui/react'
import { FastField, Form } from 'formik'
import * as React from 'react'
import { LoginProps } from 'types/app'
import LoginGoogle from './LoginGoogle'
import { useTranslation } from 'next-i18next'
const Login: React.FC<LoginProps> = ({ handleSubmit }) => {
  const { t } = useTranslation('logon')
  return (
    <Box w="100%">
      <Form>
        <FastField placeholder={t('email')} name="email" labelName={t('email')} component={theInput} />
        <FastField
          seePassword
          placeholder={t('password')}
          name="password"
          labelName={t('password')}
          component={theInput}
        />
        <Box>
          <Button
            aria-label="sing in"
            w="100%"
            mt="20px"
            p="10px 25px"
            bg="#5faed0"
            border="none"
            onClick={handleSubmit}
          >
            Sing In!
          </Button>
          {/* <Button login onClick={handleSubmit} label="Sign In!" /> */}
          <LoginGoogle />
        </Box>
      </Form>
    </Box>
  )
}

export default Login
