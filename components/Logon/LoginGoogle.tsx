import GoogleLogin from 'react-google-login'
import { Button } from '@chakra-ui/react'

const GOOGLE_ID: string | undefined = process.env.ID_APP_GOOGLE

const LoginGoogle: React.FC = () => {
  const responseGoogle = response => {
    console.log({ profile: response.profileObj })
  }

  const failedLogin = response => {
    console.log({ response })
  }
  return (
    <GoogleLogin
      clientId={GOOGLE_ID}
      render={renderProps => (
        <Button
          onClick={renderProps.onClick}
          // disabled={renderProps.disabled}
          w="100%"
          mt="15px"
          p="10px 25px"
          border="none"
          aria-label="google"
          bg="#CE4336"
        >
          Google
        </Button>
      )}
      onSuccess={responseGoogle}
      onFailure={failedLogin}
      cookiePolicy={'single_host_origin'}
    />
  )
}

export default LoginGoogle
