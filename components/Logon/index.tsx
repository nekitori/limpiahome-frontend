import { Box, Button, useColorModeValue } from '@chakra-ui/react'
import SignInSchema from '@schemas/SignInSchema'
import SignUpSchema from '@schemas/SignUpSchema'
import { CLIENT, USER_SESSION } from 'constans'
import { Formik } from 'formik'
import useRedirectSession from 'hooks/useRedirectSession'
import { useTranslation } from 'next-i18next'
import { useRouter } from 'next/router'
import React, { useLayoutEffect, useState } from 'react'
import { fetchData } from 'utils/fetchData'
import { LogonValues } from '../../types/app.d'
import Login from './Login'
import Register from './Register'

const Logon: React.FC = () => {
  const { t } = useTranslation('logon')
  const types = [t('login'), t('register')]
  const router = useRouter()
  const [active, setActive] = useState(types[0])
  const initialValues: LogonValues = {
    email: '',
    password: '',
    passwordConfirmation: ''
  }

  const { redirectSession } = useRedirectSession()

  useLayoutEffect(() => {
    redirectSession()
  }, [])

  async function signIn(data: LogonValues) {
    const response = await fetchData('login', 'put', data)
    if (response && response.status === 200) {
      const user_session = {
        token: response.data.data,
        roleName: response.data.roleName
      }

      localStorage.setItem(USER_SESSION, JSON.stringify(user_session))

      if (user_session.roleName === CLIENT) {
        router.push('/products')
      } else router.push('/admin')
    }
  }

  async function signUp(data: LogonValues) {
    const response = await fetchData('signup', 'post', data)
    if (response && response.status === 201) {
      const user_session = {
        token: response.data.data,
        roleName: response.data.roleName
      }

      localStorage.setItem(USER_SESSION, JSON.stringify(user_session))

      if (user_session.roleName === CLIENT) {
        router.push('/products')
      } else router.push('/admin')
    }
  }

  const color = useColorModeValue('blancos.100', 'primary.300')
  const theSubmit = (values: LogonValues, setSubmitting: (p: boolean) => void) => {
    const data = {
      email: values.email,
      password: values.password
    }
    active == types[0] ? signIn(data) : signUp(data)
    setSubmitting(false)
  }

  return (
    <Formik
      validationSchema={active === types[0] ? SignInSchema : SignUpSchema}
      initialValues={initialValues}
      onSubmit={(values, { setSubmitting }) => values && theSubmit(values, setSubmitting)}
    >
      {({ resetForm, handleSubmit }) => (
        <Box
          boxShadow="4px 4px 6px 1px rgba(0, 0, 0, 0.2)"
          h="100%"
          position="relative"
          display="flex"
          justifyContent="center"
          p="30px"
          alignItems="center"
          flexDirection="column"
          bg={color}
        >
          <Box position="absolute" top="0" left="0" w="100%">
            {types.map(type => (
              <Button
                fontSize="20px"
                p="10px 20px"
                cursor="pointer"
                w="50%"
                aria-label="btn-logon"
                opacity="0.5"
                border="none"
                outline="none"
                key={type}
                active={active === type}
                onClick={() => {
                  resetForm()
                  setActive(type)
                }}
              >
                {type}
              </Button>
            ))}
          </Box>

          {active == types[0] ? <Login handleSubmit={handleSubmit} /> : <Register handleSubmit={handleSubmit} />}
        </Box>
      )}
    </Formik>
  )
}

export default Logon
