import Input from '@components/Atoms/Input'
import { FastField, Form } from 'formik'
import * as React from 'react'
import { RegisterProps } from 'types/app'
import { Box, Button } from '@chakra-ui/react'
import { useTranslation } from 'next-i18next'

const Register: React.FC<RegisterProps> = ({ handleSubmit }) => {
  const { t } = useTranslation('logon')

  return (
    <Box w="100%">
      <Form>
        <FastField placeholder={t('email')} name="email" labelName={t('email')} component={Input} />
        <FastField
          seePassword
          placeholder={t('password')}
          name="password"
          labelName={t('password')}
          component={Input}
        />
        <FastField
          seePassword
          placeholder={t('passwordConfirm')}
          name="passwordConfirmation"
          labelName={t('passwordConfirm')}
          component={Input}
        />
        <Button
          aria-label="register"
          onClick={handleSubmit}
          w="100%"
          mt="20px"
          p="10px 25px"
          bg="#5faed0"
          border="none"
        >
          Sin Up!
        </Button>
      </Form>
    </Box>
  )
}

export default Register
