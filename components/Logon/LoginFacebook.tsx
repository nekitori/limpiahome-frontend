import { Button } from '@chakra-ui/react'
import { useRouter } from 'next/router'
import FacebookLogin from 'react-facebook-login'

const FACEBOOK_ID: string = process.env.ID_APP_FACEBOOK

const LoginFacebook: React.FC = () => {
  const router = useRouter()

  const responseFacebook = response => {
    console.log(response)
    router.push('/logon')
  }

  return (
    <FacebookLogin
      render={renderProps => (
        <Button aria-label="facebook" onClick={renderProps.onClick}>
          dfasdf
        </Button>
      )}
      appId={`${FACEBOOK_ID}`}
      autoLoad={false}
      fields="name,email,picture"
      callback={responseFacebook}
    />
  )
}

export default LoginFacebook
