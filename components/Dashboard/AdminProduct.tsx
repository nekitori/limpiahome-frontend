import React from 'react'
import { bgModal2 } from '../../styles/variables'
import { FaTrashAlt, FaPlusCircle, FaMinusCircle } from 'react-icons/fa'
import { Text, Box, Img, Grid, GridItem, useColorModeValue } from '@chakra-ui/react'

function AdminProduct() {
  const bg = useColorModeValue(bgModal2.light, bgModal2.dark)
  return (
    <Grid borderRadius="5px" h="7rem" templateColumns="30% 70%" p="10px" m="1rem" bg={bg}>
      <GridItem>
        <Img src="" alt="image" />
      </GridItem>
      <GridItem>
        <Box>
          <Box mb="10px" display="flex" justifyContent="space-between" alignItems="center" px="1rem">
            <Box>
              <Text>Title</Text>
              <Text>Price</Text>
            </Box>
            <FaTrashAlt fontSize="22px" />
          </Box>
          <Box display="flex" justifyContent="space-between" alignItems="center" px="1rem">
            <FaPlusCircle fontSize="22px" />
            <Text
              display="flex"
              justifyContent="center"
              alignItems="center"
              w="50px"
              h="30px"
              border="1px solid black"
              textAlign="center"
            >
              12
            </Text>
            <FaMinusCircle fontSize="22px" />
          </Box>
        </Box>
      </GridItem>
    </Grid>
  )
}

export default AdminProduct
