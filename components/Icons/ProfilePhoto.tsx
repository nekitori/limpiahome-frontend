function SvgComponent(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg width={100} height={100} viewBox="0 0 100 100" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <circle cx={50} cy={50} r={50} fill="#35F0E5" />
      <circle cx={50} cy={50} r={45.181} fill="#fff" />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M68.936 31.726A18.865 18.865 0 0150 50.655a18.867 18.867 0 01-18.936-18.929A18.864 18.864 0 0150 12.801a18.862 18.862 0 0118.936 18.925zM50 84.338c-15.515 0-28.614-2.522-28.614-12.251 0-9.733 13.181-12.165 28.614-12.165 15.518 0 28.614 2.522 28.614 12.25 0 9.733-13.181 12.166-28.614 12.166z"
        fill="#5A6160"
      />
    </svg>
  )
}

export default SvgComponent
