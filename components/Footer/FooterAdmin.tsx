import React from 'react'
import Link from 'next/link'
import { Box, Menu, MenuButton, MenuList, MenuItem, useColorModeValue } from '@chakra-ui/react'
import { GiHamburgerMenu, GiBoxUnpacking, GiShoppingBag } from 'react-icons/gi'

function FooterAdmin(): JSX.Element {
  const color = useColorModeValue('black.300', 'gris.100')
  return (
    <div>
      <Box
        p="0 2rem"
        h="100%"
        w="100%"
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        fontSize="20px"
      >
        <GiShoppingBag />
        <GiBoxUnpacking />
        <Menu>
          <MenuButton aria-label="burger">
            <GiHamburgerMenu />
          </MenuButton>
          <MenuList>
            <MenuItem color={color}>
              <Link href="/about">About</Link>
            </MenuItem>
            <MenuItem color={color}>
              <Link href="https://www.notion.so/Pol-tica-de-T-rminos-y-Condiciones-e831ae15a3bd4b9d8cfe6bd1e6e3ce9d">
                Terminos y Condiciones
              </Link>
            </MenuItem>
            <MenuItem color={color}>
              <Link href="https://www.notion.so/Aviso-de-privacidad-0c0314c35f144cf1bec9379571a24b63">
                Terminos de Privacidad
              </Link>
            </MenuItem>
            <MenuItem color={color}>
              <Link href="https://www.notion.so/Cookies-3329bcbc6ce848f1a3144cc96de0f231">Politicas de Cookies</Link>
            </MenuItem>
          </MenuList>
        </Menu>
      </Box>
    </div>
  )
}

export default FooterAdmin
