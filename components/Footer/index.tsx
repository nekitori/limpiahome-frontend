import FooterClient from './FooterClient'
import FooterAdmin from './FooterAdmin'
import FooterLog from './FooterLog'

export default { FooterClient, FooterAdmin, FooterLog }
