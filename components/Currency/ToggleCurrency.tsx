import { useAppContext } from 'hooks/useAppContext'

const ToggleCurrency = () => {
  const {
    changeCurrency,
    state: { currency }
  } = useAppContext()

  return (
    <button aria-label="currency" onClick={() => changeCurrency(currency === 'usd' ? 'mxn' : 'usd')}>
      {currency}
    </button>
  )
}

export default ToggleCurrency
