import { useRouter } from 'next/router'
import { PriceProps } from 'types/app'

const mxFormat = new Intl.NumberFormat('es-MX', { style: 'currency', currency: 'mxn' })
const coFormat = new Intl.NumberFormat('es-CO', { style: 'currency', currency: 'cop' })
const usFormat = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'usd' })

const Price: React.FC<PriceProps> = ({ children }) => {
  const { locale } = useRouter()
  let formatter

  switch (locale) {
    case 'es-MX':
      formatter = mxFormat
      break
    case 'es-MX':
      formatter = coFormat
      break
    default:
      formatter = usFormat
      break
  }

  return <span>{formatter.format(children.mxn)}</span>
}

export default Price
