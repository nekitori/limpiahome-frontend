import { ErrorMessage } from 'formik'
import React, { useState } from 'react'
import { InputProps } from '../../types/app'
import { Box, useColorModeValue, Input, InputGroup, InputRightElement, InputLeftElement } from '@chakra-ui/react'
import { FaEye, FaKey, FaMailBulk } from 'react-icons/fa'

const theInput: React.FC<InputProps> = ({ field, labelName, icon, seePassword, select, dataSelect, ...props }) => {
  const color = useColorModeValue('black.100', 'primary.200')
  const [seeText, setSeeText] = useState(!seePassword)

  const selectTag = (
    <select style={{ marginLeft: '16px' }}>
      <optgroup>
        {dataSelect &&
          dataSelect.map((data, index) => (
            <option key={index} value={data.value}>
              {data.text}
            </option>
          ))}
      </optgroup>
    </select>
  )

  return select ? (
    selectTag
  ) : (
    <Box w="100%" pb="25px">
      {labelName && <label htmlFor={labelName}>{labelName}</label>}
      <InputGroup position="relative">
        <InputLeftElement pointerEvents="none">
          {!seePassword && <FaMailBulk />}
          {seePassword && <FaKey />}
        </InputLeftElement>
        <Input
          focusBorderColor={color}
          type={seeText ? 'text' : 'password'}
          {...field}
          {...props}
          style={{ marginBottom: '10px' }}
        />
        <InputRightElement>
          {seePassword && <FaEye style={{ cursor: 'pointer' }} onClick={() => setSeeText(prev => !prev)} />}
        </InputRightElement>
        {field && (
          <Box
            fontWeight="bold"
            color="crimson"
            h="15px"
            left="2.5rem"
            top="2.6rem"
            position="absolute"
            fontSize="12px"
          >
            <ErrorMessage name={field.name} render={msg => <p>{msg}</p>} />
          </Box>
        )}
      </InputGroup>
      {/* // <Box bg="green" color="red.600" h="15px" left="1rem" top="1.7rem" position="absolute" fontSize="12px">
      //   {field && <ErrorMessage name={field.name} render={msg => <p>{msg}</p>}/>}
      // </Box> */}
    </Box>
  )
}

export default theInput
