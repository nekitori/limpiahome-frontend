import { FieldProps } from 'formik'

export interface EditTextProps extends FieldProps {
  onEdit: boolean
}
