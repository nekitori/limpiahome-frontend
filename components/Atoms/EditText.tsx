import { Field } from 'formik'
import { EditTextProps } from './types'

const EditText: React.FC<EditTextProps> = ({ onEdit, ...rest }) => {
  return <Field style={{ border: 'none', background: 'transparent' }} disabled={!onEdit} {...rest} />
}

export default EditText
