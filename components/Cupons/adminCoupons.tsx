import styled from 'styled-components'

export const Message = styled.p`
  margin-bottom: 20px;
  text-align: center;
`
