import { Box, Input, InputGroup, Select } from '@chakra-ui/react'
import { ErrorMessage } from 'formik'
import React from 'react'
interface InputCouponProps {
  select: boolean
  field?: { name: string; identifier: string }
}

const InputCoupon: React.FC<InputCouponProps> = ({ select, field, ...props }) => {
  return (
    <Box w="100%" pb="25px">
      <InputGroup position="relative">
        {select ? (
          <Select {...field} {...props} mx="1em">
            <option value="">Seleccione un descuento</option>
            <option value="10">10%</option>
            <option value="20">20%</option>
          </Select>
        ) : (
          <Input {...field} {...props} style={{ marginBottom: '10px' }} />
        )}

        {field && (
          <Box fontWeight="bold" color="crimson" h="15px" left="1.5em" top="2.6rem" position="absolute" fontSize="12px">
            <ErrorMessage name={field.name} render={msg => <p>{msg}</p>} />
          </Box>
        )}
      </InputGroup>
    </Box>
  )
}

export default InputCoupon
