import React from 'react'
import { Box, useColorModeValue } from '@chakra-ui/react'
// import FooterAdmin from '../Footer/FooterAdmin'
// import FooterClient from '../Footer/FooterClient'
import FooterLog from '../Footer/FooterLog'
import Header from '../Header'

interface LayoutProps {
  children: JSX.Element
}

const Layout: React.FC<LayoutProps> = ({ children }) => {
  const bg = useColorModeValue('blancos.500', 'primary.300')
  return (
    <>
      <Box h="100vh" w="100%" d="grid" gridTemplateRows="80px 1fr 80px">
        <Header />
        <Box bg={bg} w="100%" p="1rem" overflowX="hidden" overflowY="scroll" height="100%">
          <Box maxW="600px" m="0 auto">
            {children}
          </Box>
        </Box>
        <FooterLog />
        {/* <FooterClient /> */}
        {/* <FooterAdmin /> */}
      </Box>
    </>
  )
}

export default Layout
