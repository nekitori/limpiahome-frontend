import React, { ReactElement } from 'react'
import { Box, useColorModeValue, Heading, Text, Select } from '@chakra-ui/react'
import { bgModal, bgInput } from '@styles/variables'
interface Props {
  data?: AddressData
  type?: string
}
type AddressData = {
  id: number
  name?: string
  billing: boolean
  country?: string
  city?: string
  status?: string
  zipCode?: string
  street?: string
  externalNumber?: number
  fullName?: string
  phone?: string
  internalNumber: number
  note: string
  userId: number
}
type HeadingH5Type = {
  title: string
  content: string
}
export default function CardAddress({ data, type }: Props): ReactElement {
  const bg = useColorModeValue(bgModal.light, bgModal.dark)
  const bgi = useColorModeValue(bgInput.light, bgInput.dark)
  const FieldContent = ({ title, content }: HeadingH5Type) => (
    <Box>
      <Heading as="h5" fontSize="18px" fontWeight="500">
        {title}
      </Heading>
      <Text>{content}</Text>
    </Box>
  )
  const DeliveryCard = () => (
    <Box>
      <Box d="flex" justifyContent="space-between">
        <FieldContent title="Full Name: " content={data?.name ? data?.name : ''} />
        <FieldContent title="Phone: " content={data?.phone ? data?.phone : ''} />
      </Box>
      <Box d="flex" justifyContent="space-between" mt="1rem">
        <FieldContent title="Address: " content={data?.street ? data?.street : ''} />
        <Box>
          <Select placeholder="Select option" bg={bgi}>
            <option value="option1">Bought</option>
            <option value="option2">Sent</option>
            <option value="option3">Delivered</option>
          </Select>
        </Box>
      </Box>
    </Box>
  )
  const CardAddress = () => (
    <Box>
      <FieldContent title="Full Name: " content={data?.name ? data?.name : ''} />
      <Box d="flex" mt="1rem" justifyContent="space-between">
        <FieldContent title="Address: " content={data?.street ? data?.street : ''} />
        <FieldContent title="Phone: " content={data?.phone ? data?.phone : ''} />
      </Box>
    </Box>
  )
  return (
    <Box bg={bg} p="1rem" px="2rem" borderRadius="1em" boxShadow="2px 2px 2px rgba(0,0,0,0.2)">
      {type === 'delivery' ? <DeliveryCard /> : <CardAddress />}
    </Box>
  )
}
