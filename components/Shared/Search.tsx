import React from 'react'
import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
    Box,
    Input
  } from "@chakra-ui/react"
  import Buton from '../Shared/Button'


interface Props {
    isOpen : boolean,
    onClose: boolean
}

export const Search = ({isOpen, onClose}: Props) => {
    return (
         <>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay
        background='rgba(0,0,0, .8)'
        />
        <ModalContent
        padding='1rem'
        alignItems='center'
        >
          <ModalBody
          width='100%'
          >
            <Input placeholder='Search'/>
            <Box
            display='flex'
            justifyContent='center'
            marginTop='1rem'
            >
                <Buton label='Search' type='secondary' />
            </Box>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
    )
}
