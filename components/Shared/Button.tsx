import React, { ReactElement } from 'react'
import { Button } from '@chakra-ui/react'

interface Props {
  label?: string
  type?: string
  width?: string
  onClick?: () => void
}

export default function Buton({ label, type, width, onClick }: Props): ReactElement {
  const BtnPrimary = () => (
    <Button
      aria-label={label}
      bg="secondary.300"
      fontSize="1rem"
      height="50px"
      width={width ? '100%' : '200px'}
      border="1px solid secondary.200"
      onClick={() => onClick && onClick()}
      boxShadow="
              0 4px 4px rgba(0, 0, 0, .25), 
              inset 0 4px 3px rgba(255, 255, 255, .65),
              inset 0 -4px 2px secondary.200"
      _hover={{
        width: width ? '100%' : '210px',
        fontSize: '18px',
        color: 'secondary.200',
        border: '1px solid secondary.300',
        backgroundColor: 'secondary.400',
        boxShadow:
          '0 4px 4px rgba(0, 0, 0, .25), inset 0 4px 3px rgba(255, 255, 255, .65), inset 0 -4px 2px secondary.300'
      }}
      _active={{
        width: width ? '100%' : '190px',
        fontSize: '1rem',
        color: 'blancos.100',
        border: '1px solid secondary.100',
        backgroundColor: 'secondary.200',
        boxShadow:
          '0 4px 4px rgba(0, 0, 0, .25), inset 0 4px 3px rgba(255, 255, 255, .65), inset 0 -4px 2px secondary.100'
      }}
    >
      {label}
    </Button>
  )

  const BtnSecondary = () => (
    <Button
      aria-label={label}
      bg="primary.900"
      fontSize="1rem"
      height="50px"
      width={width ? '100%' : '200px'}
      onClick={() => onClick && onClick()}
      border="1px solid primary.100"
      boxShadow="
        0 4px 4px rgba(0, 0, 0, .25), 
        inset 0 4px 3px rgba(255, 255, 255, .65),
        inset 0 -4px 2px primary.100"
      _hover={{
        width: width ? '100%' : '210px',
        fontSize: '18px',
        color: 'primary.100',
        border: '1px solid primary.900',
        backgroundColor: 'primary.500',
        boxShadow:
          '0 4px 4px rgba(0, 0, 0, .25), inset 0 4px 3px rgba(255, 255, 255, .65), inset 0 -4px 2px primary.900'
      }}
      _active={{
        width: width ? '100%' : '190px',
        fontSize: '1rem',
        color: 'blancos.100',
        border: '1px solid primary.200',
        backgroundColor: 'primary.100',
        boxShadow:
          '0 4px 4px rgba(0, 0, 0, .25), inset 0 4px 3px rgba(255, 255, 255, .65), inset 0 -4px 2px primary.200'
      }}
    >
      {label}
    </Button>
  )

  return <>{type === 'primary' ? <BtnPrimary /> : <BtnSecondary />}</>
}
