import React, { useState } from 'react'
import { Box, Heading, Divider } from '@chakra-ui/react'
import Step0 from '@components/Checkout/Steps/Step0'
import Step1 from '@components/Checkout/Steps/Step1'
import Step2 from '@components/Checkout/Steps/Step2'

export default function Steps({}: Props): ReactElement {
  const [actualS, setActualS] = useState(1)
  const [ableToMove, setAbleToMove] = useState(1)
  const handleChange = (val: number, type: string) => {
    if (type === 'step') {
      ableToMove >= val && setActualS(val)
    } else {
      setAbleToMove(actualS + 1)
      setActualS(val)
    }
    window.scrollTo(0, 0)
  }
  const Step = ({ label, status }: StepT) => (
    <Box
      bg={
        status == 'done' ? 'primary.500' : status === 'active' ? 'primary.300' : status === 'wait' ? 'gris.500' : 'none'
      }
      borderRadius="2em"
      px="1em"
      color="blancos.100"
      fontWeight="900"
      py="0.5em"
      fontSize="24px"
      onClick={() => handleChange(parseInt(label), 'step')}
    >
      {label}
    </Box>
  )
  const Line = ({ type }: LineT) => (
    <Divider borderColor={type === 'active' ? 'primary.500' : 'gris.500'} borderWidth="4px" w="100px" />
  )
  return (
    <Box>
      <Box id="owo">
        <Heading as="h1" fontFamily="22px">
          {actualS === 1 ? 'Confirm Info' : actualS === 2 ? 'Checkout' : actualS === 3 && 'Payment'}
        </Heading>
        <Box d="flex" w="90%" justifyContent="center" m="0 auto" my="1em" mb="2em" alignItems="center">
          <Step label="1" status={actualS === 1 ? 'active' : actualS > 1 ? 'done' : 'wait'} />
          <Line type="active" />
          <Step label="2" status={actualS === 2 ? 'active' : actualS < 2 ? 'wait' : 'done'} />
          <Line type={actualS > 1 ? 'active' : 'wait'} />
          <Step label="3" status={actualS === 3 ? 'active' : actualS > 3 ? 'done' : 'wait'} />
        </Box>
        <Box>
          {actualS === 1 ? (
            <Step1 handleChange={() => handleChange(2, '')} />
          ) : actualS === 2 ? (
            <Step2 back={() => handleChange(1, 'btn')} handleChange={() => handleChange(3, 'btn')} />
          ) : (
            actualS === 3 && <Step0 back={() => handleChange(2, 'btn')} handleChange={() => handleChange(1, 'btn')} />
          )}
        </Box>
      </Box>
    </Box>
  )
}
