# Limpiahome E-commerce 🧼

Tienda online para envases ecologicos. Sin preocuparte de comprar nuevos productos de limpieza, te damos la opcion de rellenar mensualmente o quedarte con tu kit de preferencia.
## Status

---

En desarrollo, Ecommerce.

## Getting Started 🛸

---

> Survivor Kit

| Technology | Documentation                                      |
| ---------- | -------------------------------------------------- |
| Node       | https://www.npmjs.com/get-npm                      |
| Yarn       | https://legacy.yarnpkg.com/lang/en/docs/install/   |
| Prettier   | https://prettier.io/docs/en/install.html           |
| Eslint     | https://eslint.org/docs/user-guide/getting-started |

---

> Installation

```sh
$ yarn install
```

---

> Local

```sh
$ yarn dev
```

---

> Deploy

```
$ yarn start
$ yarn build
```

---

> Testing

```sh
$ yarn test
```

## Issues 🚿

---

> Errors

> Bugs

> FAQ

## Versioning

---

Node v14.15.1

Yarn v1.22.10.

Limpiahome v0.1.35

## Authors 🧽

---

| [Victor Garibay](https://github.com/VictorGaribay) |
[Antonio Jimenez](https://github.com/Tonnraus) |
[Kevin Malaver](https://github.com/kevinmalaverr) |
[Luis Isasi](https://github.com/luis-isasi) |
