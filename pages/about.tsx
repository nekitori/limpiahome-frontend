import Head from 'next/head'
import { Box, Heading, Text, useColorModeValue } from '@chakra-ui/react'
import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

import { bgModal } from '../styles/variables'
const About: React.FC = () => {
  const bg = useColorModeValue(bgModal.light, bgModal.dark)
  const { t } = useTranslation('about')

  return (
    <>
      <Head>
        <title>About Us</title>
      </Head>
      <Box w="100%" d="flex" flexFlow="column" alignItems="center" p="1rem">
        <Heading fontSize="30px" as="h1" my="25px">
          {t('h1')}
        </Heading>
        <Box>
          <Text bg={bg} borderRadius="0.5em" p="1rem" fontSize="lg">
            <Text p="1rem">{t('p1')}</Text>
            <Text p="1rem">{t('p2')}</Text>
            <Text p="1rem">{t('p3')}</Text>
            <Text p="1rem">{t('p4')}</Text>
            <Text p="1rem">{t('p5')}</Text>
            <Text p="1rem">{t('p6')}</Text>
          </Text>
        </Box>
      </Box>
    </>
  )
}

export const getStaticProps: (locale: any) => void = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['about']))
  }
})

export default About
