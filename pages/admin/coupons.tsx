import Head from 'next/head'
import { Box, Heading, IconButton, ListItem, Text, UnorderedList, useColorModeValue } from '@chakra-ui/react'
import Alert from '@components/CheckList/Alert'
import { Message } from '@components/Cupons/adminCoupons'
import InputCoupon from '@components/Cupons/InputCoupon'
import CouponsSchema from '@schemas/CouponsSchema'
import { bgModal } from '@styles/variables'
import { FastField, Form, Formik } from 'formik'
import { useAppContext } from 'hooks/useAppContext'
import useRedirectSession from 'hooks/useRedirectSession'
import * as React from 'react'
import { RiAddFill } from 'react-icons/ri'
import { Coupon } from 'types/app'
interface NewCoupon {
  code: string
  discount: string
}
type CardCouponType = {
  label: string
}
const Coupons: React.FC = () => {
  const {
    addCoupon,
    deleteCoupon,
    state: { coupons }
  } = useAppContext()
  const { redirectAdmin } = useRedirectSession()
  React.useLayoutEffect(() => {
    redirectAdmin()
  }, [])
  const renderCupons10Admin = (typeCupon: string) => {
    const filterCoupons: Coupon[] = []
    coupons.forEach((coupon: Coupon) => {
      const { descount } = coupon
      if (descount === typeCupon) filterCoupons.push(coupon)
    })
    if (!filterCoupons.length) {
      return <Message>No hay cupones ingresados</Message>
    }
    return filterCoupons.map((coupon: Coupon) => {
      const { descount } = coupon
      if (descount === typeCupon) {
        return <Li key={coupon.id} code={coupon.code} id={coupon.id} />
      }
    })
  }
  const Li = ({ key, code, id }) => (
    <ListItem key={key} d="flex" mt="0.5em" justifyContent="space-between" px="1rem">
      <Text>{code}</Text>
      <Alert handleRemoveFromCart={() => deleteCoupon(id)} />
    </ListItem>
  )
  const theSubmit = (values: NewCoupon, setSubmitting: (p: boolean) => void, resetForm) => {
    addCoupon(values.code, values.discount)
    resetForm()
    setSubmitting(false)
  }
  const CardCoupons = ({ label }: CardCouponType) => (
    <Box bg={bg} boxShadow="rgba(0,0,0,0.2) 2px 2px 2px" p="1rem" borderRadius="1em" my="0.5em">
      <Heading>{label}</Heading>
      <UnorderedList>
        {label === '10%' ? renderCupons10Admin('10') : label === '20%' && renderCupons10Admin('20')}
      </UnorderedList>
    </Box>
  )
  const bg = useColorModeValue(bgModal.light, bgModal.dark)
  return (
    <>
      <Head>
        <title>Admin | Coupons</title>
      </Head>

      <Box>
        <Heading as="h1">Coupons</Heading>
        <Box bg={bg} p="1rem" borderRadius="1em" my="1em">
          <Formik
            validationSchema={CouponsSchema}
            initialValues={{ code: '', discount: '' }}
            onSubmit={(values, { setSubmitting, resetForm }) => values && theSubmit(values, setSubmitting, resetForm)}
          >
            {({ handleSubmit }) => (
              <Form>
                <Box d="flex">
                  <FastField placeholder="Type a new discount code..." name="code" component={InputCoupon} />
                  <FastField select name="discount" component={InputCoupon} />
                  <IconButton aria-label="add more" onClick={() => handleSubmit()} icon={<RiAddFill size="16px" />} />
                </Box>
              </Form>
            )}
          </Formik>
        </Box>
        <CardCoupons label="10%" />
        <CardCoupons label="20%" />
      </Box>
    </>
  )
}
export default Coupons
