import * as React from 'react'
import Head from 'next/head'

import { useAppContext } from 'hooks/useAppContext'
import useRedirectSession from 'hooks/useRedirectSession'
import CardAddress from '@components/Shared/CardAddress'

import { Box, Heading, Input, useColorModeValue } from '@chakra-ui/react'
import { bgInput } from '@styles/variables'

type AddressData = {
  id: number
  name?: string
  billing: boolean
  country?: string
  city?: string
  status?: string
  zipCode?: string
  street?: string
  externalNumber?: number
  fullName?: string
  phone?: string
  internalNumber: number
  note: string
  userId: number
}

const data: AddressData[] = [
  {
    id: 2,
    name: 'Casa de mis padres',
    billing: true,
    fullName: 'keanu reeves',
    phone: '5555555555',
    country: 'USA',
    status: 'buyed',
    city: 'los angeles',
    zipCode: '09220',
    street: 'street',
    externalNumber: 666,
    internalNumber: 8,
    note: 'Cuidado con el perro',
    userId: 1
  },
  {
    id: 2,
    name: 'Casa de mis padres',
    billing: true,
    fullName: 'keanu reeves',
    status: 'buyed',
    phone: '5555555555',
    country: 'USA',
    city: 'los angeles',
    zipCode: '09220',
    street: 'street',
    externalNumber: 666,
    internalNumber: 8,
    note: 'Cuidado con el perro',
    userId: 1
  }
]
const Delivery = () => {
  const {
    state: {
      buyer: { rol }
    }
  } = useAppContext()
  const { redirectAdmin } = useRedirectSession()
  React.useLayoutEffect(() => {
    redirectAdmin()
  }, [])

  const bgi = useColorModeValue(bgInput.light, bgInput.dark)
  const mappingCards = (p: AddressData, i: number) => (
    <Box key={i} mt="1rem">
      <CardAddress data={p} type="delivery" />
    </Box>
  )
  return (
    <>
      <Head>
        <title>Admin | Delivery</title>
      </Head>
      <Box>
        <Heading as="h1"> Delivery</Heading>
        <Input bg={bgi} placeholder="Search order" mt="1em" />
        <Box mt="1em">{data.map(mappingCards)}</Box>
        <Box></Box>
      </Box>
    </>
  )
}

export default Delivery
