import { useState, useLayoutEffect } from 'react'
import Head from 'next/head'
import CategoriesForm from 'components/Forms/CategoriesForm'
import { useGetState } from 'hooks/useGetState'
import useRedirectSession from 'hooks/useRedirectSession'
import { FaEdit, FaPlus, FaTrash } from 'react-icons/fa'
import {
  Button,
  Box,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableCaption,
  useDisclosure,
  IconButton,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton
} from '@chakra-ui/react'

const Categories: React.FC = () => {
  const categories = useGetState('categories')
  const { isOpen, onOpen, onClose } = useDisclosure()
  const [CategoryIdEdit, setCategoryIdEdit] = useState(false)
  const [selectedId, setSelectedId] = useState<number | undefined>()

  const { redirectAdmin } = useRedirectSession()

  useLayoutEffect(() => {
    redirectAdmin()
  }, [])

  const addCategory = () => {
    onOpen()
  }

  const deleteCategory = (id: number) => {
    alert(`delete Category ${id}`)
  }

  // const editCategory = () => {
  //   setCategoryIdEdit(true)
  //   onOpen()
  // }
  const handleEdit = (id: number) => {
    setSelectedId(id)
    onOpen()
  }

  const ModalC = () => (
    <Modal isOpen={isOpen} isCentered onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Modal Title</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <CategoriesForm editId={selectedId} />
        </ModalBody>
        <ModalFooter>
          <Button aria-label="close-categories" isFullWidth colorScheme="blue" mr={3} onClick={onClose}>
            Close
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  )

  return (
    <>
      <Head>
        <title>Admin | Categories</title>
      </Head>
      <Box w="100%" overflowX="scroll">
        <Button aria-label="icon-plus" onClick={addCategory}>
          <FaPlus />
        </Button>
        <ModalC />

        <Box>
          <Table variant="simple">
            <TableCaption>Categories</TableCaption>
            <Thead>
              <Tr>
                <Th>id</Th>
                <Th>name</Th>
                <Th>action</Th>
              </Tr>
            </Thead>
            <Tbody>
              {categories &&
                categories.map(({ id, name }: { id: number; name: string }) => (
                  <Tr key={id}>
                    <Td>{id}</Td>
                    <Td>{name}</Td>
                    <Td>
                      <IconButton aria-label="edit" onClick={() => handleEdit(id)}>
                        <FaEdit />
                      </IconButton>
                      <IconButton aria-label="delete" ml="10px" onClick={() => deleteCategory(id)}>
                        <FaTrash />
                      </IconButton>
                    </Td>
                  </Tr>
                ))}
            </Tbody>
          </Table>
        </Box>
      </Box>
    </>
  )
}

export default Categories
