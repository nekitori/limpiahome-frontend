import Head from 'next/head'

import React, { useLayoutEffect } from 'react'
import { bgModal } from '../../styles/variables'
import Link from 'next/link'
import useRedirectSession from 'hooks/useRedirectSession'
import AdminProduct from '../../components/Dashboard/AdminProduct'
import { FaPlus, FaGift, FaLayerGroup } from 'react-icons/fa'
import { Box, Text, Grid, GridItem, useColorModeValue } from '@chakra-ui/react'
import { ErrorMessage } from 'formik'

const index: React.FC = () => {
  const bg = useColorModeValue(bgModal.light, bgModal.dark)
  const { redirectAdmin } = useRedirectSession()

  useLayoutEffect(() => {
    redirectAdmin()
  }, [])
  return (
    <>
      <Head>
        <title>Admin | Dashboard</title>
      </Head>
      <Grid templateRows="4rem 1fr">
        <GridItem display="flex" justifyContent="center" alignItems="center" flexGrow={1}>
          <Text fontSize="28px" w="100%">
            Products
          </Text>
          <Box fontSize="30px" w="100%" display="flex" justifyContent="space-evenly">
            <Link href="/admin/products">
              <FaPlus style={{ cursor: 'pointer' }} />
            </Link>
            <Link href="/admin/coupons">
              <FaGift style={{ cursor: 'pointer' }} />
            </Link>
            <Link href="/admin/categories">
              <FaLayerGroup style={{ cursor: 'pointer' }} />
            </Link>
          </Box>
        </GridItem>
        <GridItem bg={bg}>
          <AdminProduct />
          <AdminProduct />
          <AdminProduct />
        </GridItem>
      </Grid>
    </>
  )
}

export default index
