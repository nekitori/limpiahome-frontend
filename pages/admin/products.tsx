import Head from 'next/head'
import { useState, useLayoutEffect } from 'react'
import ProductsForm from '@components/Forms/ProductsForm'
import { useGetState } from 'hooks/useGetState'
import useRedirectSession from 'hooks/useRedirectSession'
import { FaEdit, FaTrash, FaPlus } from 'react-icons/fa'
import {
  Button,
  Box,
  Table,
  Thead,
  Tbody,
  Tfoot,
  Tr,
  Th,
  Td,
  TableCaption,
  useDisclosure,
  IconButton,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton
} from '@chakra-ui/react'

const Products: React.FC = () => {
  const products = useGetState('products')
  const { isOpen, onOpen, onClose } = useDisclosure()
  const [productIdEdit, setProductIdEdit] = useState(false)

  const { redirectAdmin } = useRedirectSession()

  useLayoutEffect(() => {
    redirectAdmin()
  }, [])

  const addProduct = () => {
    setProductIdEdit(false)
    onOpen()
  }

  const deleteProduct = (productId: number) => {
    // alert(`delete product ${productId}`)
  }

  const editProduct = (productId: number) => {
    setProductIdEdit(productId)
    onOpen()
  }
  const ModalC = () => (
    <Modal isOpen={isOpen} isCentered onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Modal Title</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <ProductsForm editId={productIdEdit} />
        </ModalBody>
        <ModalFooter>
          <Button aria-label="close-products" isFullWidth colorScheme="blue" mr={3} onClick={onClose}>
            Close
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  )

  return (
    <>
      <Head>
        <title>Admin | Products</title>
      </Head>

      <Box w="100%" overflowX="scroll">
        <Button onClick={addProduct}>
          <FaPlus />
        </Button>
        <ModalC />
        <Table variant="simple">
          <TableCaption>Products TableEND</TableCaption>
          <Thead>
            <Tr>
              <Th>id</Th>
              <Th>title</Th>
              <Th>description</Th>
              <Th>price</Th>
              <Th>action</Th>
            </Tr>
          </Thead>
          <Tbody>
            {products?.map(({ id, name, description, price }) => (
              <Tr key={id}>
                <Td>{id}</Td>
                <Td>{name}</Td>
                <Td>{description}</Td>
                <Td>{price}</Td>
                <Td display="flex">
                  <IconButton mr="10px" aria-label="edit" onClick={() => editProduct(id)}>
                    <FaEdit />
                  </IconButton>
                  <IconButton aria-label="trash" onClick={() => deleteProduct(id)}>
                    <FaTrash />
                  </IconButton>
                </Td>
              </Tr>
            ))}
          </Tbody>
          <Tfoot>
            <Tr>
              <Th>END</Th>
              <Th>END</Th>
              <Th>END</Th>
              <Th>END</Th>
              <Th>END</Th>
            </Tr>
          </Tfoot>
        </Table>
      </Box>
    </>
  )
}

export default Products
