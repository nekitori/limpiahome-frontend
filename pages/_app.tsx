import Layout from '@components/Shared/Layout'
import { appWithTranslation } from 'next-i18next'
import { AppProps } from 'next/app'
import React from 'react'

// chakra
import { ChakraProvider } from '@chakra-ui/react'
import tema from '../styles/tema'

//
import { AppContextProvider } from '../context/AppContext'

const MyApp: React.FC<AppProps> = ({ Component, pageProps }) => {
  return (
    <AppContextProvider>
      <ChakraProvider theme={tema}>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </ChakraProvider>
    </AppContextProvider>
  )
}

export default appWithTranslation(MyApp)
