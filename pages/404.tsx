import Head from 'next/head'
import Link from 'next/link'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useTranslation } from 'next-i18next'

import { Box, Text, Image, Grid, GridItem } from '@chakra-ui/react'
import Button from '../components/Shared/Button'
const notFound: React.FC = () => {
  const { t } = useTranslation()
  return (
    <Grid templateRows="2fr 1fr 1fr" height="100%">
      <GridItem>
        <Image width="100%" height="100%" backgroundColor="primary.200" src="" />
      </GridItem>
      <GridItem textAlign="center" padding="1rem 0 0 0">
        <Text fontWeight="700" fontSize="30px">
          404 Error
        </Text>
        <Text>{t('notFound.message')}</Text>
      </GridItem>
      <GridItem display="flex" justifyContent="center" padding="2rem">
        <Link href="/">
          <a>
            <Button label="Go home" type="primary" />
          </a>
        </Link>
      </GridItem>
    </Grid>
  )
}
export default notFound

export const getStaticProps: (locale: any) => void = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common']))
  }
})
