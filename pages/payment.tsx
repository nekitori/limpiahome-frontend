import Head from 'next/head'
import { useLayoutEffect } from 'react'
import useRedirectSession from 'hooks/useRedirectSession'

function Payment() {
  const { redirectToLogon } = useRedirectSession()

  useLayoutEffect(() => {
    redirectToLogon()
  }, [])
  return (
    <>
      <Head>
        <title>Payment</title>
      </Head>
      <div>Payment</div>
    </>
  )
}

export default Payment
