import Head from 'next/head'
import React, { useState, useEffect } from 'react'
import { useAppContext } from 'hooks/useAppContext'
import SwipeCard from '@components/Products/swipeCard'
import Category from 'components/Products/Category'
import Main from '@components/Products/Main'
import { fetchData } from 'utils/fetchData'
import { useGetCurrency } from 'hooks/useGetCurrency'

const Tinder = (): JSX.Element => {
  const { state, addToCart, addCategoriesAndProducts } = useAppContext()
  const currency = useGetCurrency()
  const [category, setCategory] = useState()
  const products = state?.products

  useEffect(() => {
    async function get() {
      const [products, categories] = await Promise.all([
        fetchData(`products?lang=${currency}`),
        fetchData(`products/categories?lang=${currency}`)
      ])
      if (products?.status === 200 && categories?.status === 200) {
        addCategoriesAndProducts(categories.data.data, products.data.data)
      }
    }
    get()
  }, [currency])

  const handleAddToCart = (id: number, side: string) => {
    if (addToCart && products && side == 'right') {
      addToCart(products.find(product => product.id == id))
      console.log(state)
    }
  }

  const handleChangeCategory = e => {
    setCategory(parseInt(e.target.options[e.target.selectedIndex].value))
  }

  let productsId
  console.log(category)
  if (category) {
    productsId = products?.filter(product => product.categoryId == category).map(product => product.id)
  } else {
    productsId = products?.map(product => product.id)
  }

  console.log(products)

  console.log(productsId?.length)

  return (
    <>
      <Head>
        <title>Products</title>
      </Head>
      <Main>
        <Category onChange={handleChangeCategory} />
        {productsId ? <SwipeCard productsId={productsId} addToCart={handleAddToCart} /> : null}
      </Main>
    </>
  )
}

export default Tinder
