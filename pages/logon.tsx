import Head from 'next/head'
import { Box, Button, useColorModeValue } from '@chakra-ui/react'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import SignInSchema from '@schemas/SignInSchema'
import SignUpSchema from '@schemas/SignUpSchema'
import { CLIENT, USER_SESSION } from 'constans'
import { Formik } from 'formik'
import useRedirectSession from 'hooks/useRedirectSession'
import { useRouter } from 'next/router'
import React, { useLayoutEffect, useState } from 'react'
import { fetchData } from 'utils/fetchData'
import { LogonValues } from '../types/app.d'
import Login from '../components/Logon/Login'
import Register from '../components/Logon/Register'

const Logon: React.FC = () => {
  const types = ['Iniciar sesión', 'Registrate']
  const router = useRouter()
  const [active, setActive] = useState(types[0])
  const initialValues: LogonValues = {
    email: '',
    password: '',
    passwordConfirmation: ''
  }

  const { redirectSession } = useRedirectSession()

  useLayoutEffect(() => {
    redirectSession()
  }, [])

  async function signIn(data: LogonValues) {
    const response = await fetchData('login', 'put', data)
    if (response && response.status === 200) {
      const user_session = {
        token: response.data.data,
        roleName: response.data.roleName
      }

      localStorage.setItem(USER_SESSION, JSON.stringify(user_session))

      if (user_session.roleName === CLIENT) {
        router.push('/products')
      } else router.push('/admin')
    }
  }

  async function signUp(data: LogonValues) {
    const response = await fetchData('signup', 'post', data)
    if (response && response.status === 201) {
      const user_session = {
        token: response.data.data,
        roleName: response.data.roleName
      }

      localStorage.setItem(USER_SESSION, JSON.stringify(user_session))

      if (user_session.roleName === CLIENT) {
        router.push('/products')
      } else router.push('/admin')
    }
  }

  const color = useColorModeValue('blancos.100', 'primary.300')
  const theSubmit = (values: LogonValues, setSubmitting: (p: boolean) => void) => {
    const data = {
      email: values.email,
      password: values.password
    }
    active == 'Iniciar sesión' ? signIn(data) : signUp(data)
    setSubmitting(false)
  }

  return (
    <>
      <Head>
        <title>Logon</title>
      </Head>
      <Formik
        validationSchema={active === 'Iniciar sesión' ? SignInSchema : SignUpSchema}
        initialValues={initialValues}
        onSubmit={(values, { setSubmitting }) => values && theSubmit(values, setSubmitting)}
      >
        {({ resetForm, handleSubmit }) => (
          <Box
            boxShadow="4px 4px 6px 1px rgba(0, 0, 0, 0.2)"
            h="100%"
            position="relative"
            display="flex"
            justifyContent="center"
            p="30px"
            alignItems="center"
            flexDirection="column"
            bg={color}
          >
            <Box position="absolute" top="0" left="0" w="100%">
              {types.map(type => (
                <Button
                  fontSize="20px"
                  p="10px 20px"
                  cursor="pointer"
                  w="50%"
                  opacity="0.5"
                  border="none"
                  outline="none"
                  key={type}
                  active={active === type}
                  onClick={() => {
                    resetForm()
                    setActive(type)
                  }}
                >
                  {type}
                </Button>
              ))}
            </Box>

            {active == 'Iniciar sesión' ? (
              <Login handleSubmit={handleSubmit} />
            ) : (
              <Register handleSubmit={handleSubmit} />
            )}
          </Box>
        )}
      </Formik>
    </>
  )
}

export default Logon

export const getStaticProps: (locale: any) => void = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['logon']))
  }
})
