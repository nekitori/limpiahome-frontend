import Head from 'next/head'
import { useState } from 'react'
import Information from '../components/Profile/Information'
import PaidData from '../components/Profile/PaidData'
import Security from '../components/Profile/Security'
import { Box, Button, useColorModeValue } from '@chakra-ui/react'

const tabs = [<Information key={0} />, <PaidData key={1} />, <Security key={2} />]

const Profile: React.FC = () => {
  const [on, setOn] = useState(0)

  const dark = useColorModeValue('gris.400', 'primary.300')
  const darker = useColorModeValue('gris.200', 'primary.200')

  return (
    <>
      <Head>
        <title>Profile Settings</title>
      </Head>
      <Box minH="100%" display="flex" flexDirection="column" flexGrow={1}>
        <Box display="flex" justifyContent="stretch">
          <Button
            bg={on === 0 ? dark : darker}
            position="relative"
            borderRadius="10px 10px 0px 0px"
            flexGrow={1}
            display="block"
            fontWeight="bold"
            aria-label="information"
            fontSize="16px "
            p="0.4rem"
            zIndex={`${() => (on ? '10' : '0')}`}
            outline="none"
            onClick={() => setOn(0)}
            borderBottom={on === 0 ? 'none' : '1px solid #cecece'}
          >
            Information
          </Button>
          <Button
            bg={on === 1 ? dark : darker}
            borderRadius="10px 10px 0px 0px"
            position="relative"
            flexGrow={1}
            display="block"
            fontWeight="bold"
            aria-label="paid-data"
            fontSize="16px"
            p="0.4rem"
            zIndex={`${() => (on ? '10' : '0')}`}
            outline="none"
            onClick={() => setOn(1)}
            borderBottom={on === 1 ? 'none' : '1px solid #cecece'}
          >
            Paid Data
          </Button>
          <Button
            bg={on === 2 ? dark : darker}
            borderRadius="10px 10px 0px 0px"
            position="relative"
            flexGrow={1}
            display="block"
            aria-label="security"
            fontWeight="bold"
            fontSize="16px"
            p="0.4rem"
            zIndex={`${() => (on ? '10' : '0')}`}
            outline="none"
            onClick={() => setOn(2)}
            borderBottom={on === 2 ? 'none' : '1px solid #cecece'}
          >
            Security
          </Button>
        </Box>
        <section>
          <Box flexGrow={1} borderTop="none" borderRadius="0px" p="1rem" boxShadow="0 3px 5px rgba(0, 0, 0, 0.25)">
            {tabs[on]}
          </Box>
        </section>
      </Box>
    </>
  )
}

export default Profile
