import Head from 'next/head'
import Link from 'next/link'
import React from 'react'
import { Box, Text, Image, Grid, GridItem, useColorModeValue} from '@chakra-ui/react'
// import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import Button from '../components/Shared/Button'
// import { ContentHome, Btn } from '@components/Home/home'
import {fonts} from  '../styles/variables'
import SachetImg from '../public/img/sachet.png'
import Texture1 from '../public/img/texture1.png'
import Texture2 from '../public/img/texture2.png'



const Home: React.FC = () => {
  // const price = { usd: 10, mxn: 700 }
  // const { t } = useTranslation()
  const image = useColorModeValue('https://i.ibb.co/n65CqzG/Asset-3.png', "https://i.ibb.co/n6R7SrJ/text.png" )
  return (
    <>
      <Head>
        <title>Homepage</title>
      </Head>
      {/*NO SÉ DE QUIEN ES ESTO POR ESO NO LO BORRE*/}
      {/* <h1>{t('home.welcome')}</h1>
      <h2>
        {t('home.price')}: <Price>{price}</Price>
      </h2> */}
      <Grid
      width= "100%"
      height= "100%"
      templateRows='1fr 3fr 1fr'
      // display= "flex"
      // flexFlow= "column"
      // justifyContent= "space-between"
      // alignItems= "center"
      >
        <GridItem
        display='flex'
        alignItems='flex-end'
        position='relative'
        >
          <Text
          fontSize='42px'
          fontWeight='700'
          >
            Menos Plásticos, Más Tiempo para tí
          </Text>
        </GridItem>
        {/* <Box color="secondary.100" fontSize="60px">
          Hola{' '}
        </Box> */}
        <GridItem
        display='flex'
        justifyContent='center'
        position='relative'
        >
          <Box
          position='absolute'
          bottom='20px'
          right='10px'
          zIndex='3'
          transform='rotateY(180deg)'
          >
            <Image 
            src={image}
            width='150px'
            alt='image of the product'
            />
          </Box>
          <Box
          position='absolute'
          bottom='-80px'
          left='-50px'
          transform='rotateY(180deg)'
          opacity='.3'
          >
            <Image 
            src='https://i.ibb.co/wg5BdMQ/texture1.png'
            width='150px'
            alt='textureImage'
            />
          </Box>
          <Box
          position='absolute'
          left='20px'
          bottom='0px'
          >
            <Image
            height='300px'
            src='https://i.ibb.co/kx57YDx/sachet-Home.png'
            alt='image of a sachet'
            />
          </Box>
          <Box
          position='absolute'
          top='0px'
          right='-20px'
          opacity='.4'
          transform='rotateY(180deg)'
          >
            <Image 
            src='https://i.ibb.co/NF4bDHS/texture2.png'
            width='150px'
            alt='textureImage'
            />
          </Box>
        </GridItem>
        <GridItem
        width='100%'
        >
          <Box
          marginBottom="1rem"
          display='flex'
          justifyContent='center'
          >
            <Link href="/logon">
              <a>
                <Button label='Sign Up' type='primary' />
              </a>
            </Link>
          </Box>
          <Box
          display='flex'
          justifyContent='center'
          >
            <Link href="/about">
              <Box>
                <Button label="About" type="secondary" />
              </Box>
            </Link>
          </Box>
        </GridItem>
      </Grid>
    </>
  )
}

export const getStaticProps: (locale: any) => void = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common']))
  }
})

export default Home
