import Head from 'next/head'
import { Box } from '@chakra-ui/react'
import Steps from '@components/Shared/Steps'
import useRedirectSession from 'hooks/useRedirectSession'
import React, { useEffect } from 'react'

const Checkout: React.FC = ({}) => {
  const { redirectToLogon } = useRedirectSession()
  useEffect(() => {
    redirectToLogon()
  }, [])

  return (
    <>
      <Head>
        <title>Checkout</title>
      </Head>
      <Box minH="100vh">
        <Steps />
      </Box>
    </>
  )
}

export default Checkout
