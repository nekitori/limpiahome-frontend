import Head from 'next/head'
import Link from 'next/link'
import { Text, Image, Grid, GridItem } from '@chakra-ui/react'
import Button from '../components/Shared/Button'
const Success: React.FC = () => {
  return (
    <>
      <Head>
        <title>Success Payment</title>
      </Head>
      <Grid templateRows="2fr 1fr 1fr" height="100%">
        <GridItem>
          <Image width="100%" height="100%" backgroundColor="primary.200" src="" />
        </GridItem>
        <GridItem textAlign="center" padding="1rem 0 0 0">
          <Text fontWeight="700" fontSize="30px">
            Felicidades!
          </Text>
          <Text>con tu compra acabas de s alvar dos osos polares</Text>
        </GridItem>
        <GridItem display="flex" justifyContent="center" padding="2rem">
          <Link href="/">
            <a>
              <Button label="Go home" type="primary" />
            </a>
          </Link>
        </GridItem>
      </Grid>
    </>
  )
}
export default Success
