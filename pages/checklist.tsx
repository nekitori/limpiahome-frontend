import Head from 'next/head'
import React, { useLayoutEffect, useEffect, useState } from 'react'
import { Box } from '@chakra-ui/react'
import Button from '@components/Shared/Button'
import Card from '@components/CheckList/Card'
import Coupon from '@components/Coupon'
import { useAppContext } from 'hooks/useAppContext'
import { useRouter } from 'next/router'
import { ProductsType } from '../types/products'
import useRedirectSession from 'hooks/useRedirectSession'

interface ValueProps {
  discount: number
  total: number
  subTotal: number
}

const Checkout: React.FunctionComponent = () => {
  const router = useRouter()
  const [valueProduct, setValueProduct] = useState<ValueProps>({ discount: 0, total: 0, subTotal: 0 })
  const { state, removeFromCart, subtractQuantity, addQuantity } = useAppContext()
  const cart = state?.cart

  const { redirectToLogon } = useRedirectSession()

  useLayoutEffect(() => {
    redirectToLogon()
  }, [])

  useEffect(() => {
    handleSubTotal()
    localStorage.setItem('valueProducts', JSON.stringify(valueProduct))
  }, [cart])

  const handleRemoveFromCart = (product: ProductsType, i: number) => {
    if (removeFromCart) {
      removeFromCart(product, i)
    }
  }

  const handleAddQuantity = (product: ProductsType) => {
    if (addQuantity) {
      addQuantity(product)
    }
  }

  const handleSubtractQuantity = (product: ProductsType) => {
    if (subtractQuantity) {
      subtractQuantity(product)
    }
  }

  const handleSubTotal = () => {
    const reducer: (lastPrice: number, item: ProductsType) => number = (lastPrice, item) => {
      const quantity = item?.quantity
      const price = item?.price
      const total = price * quantity
      if (total) {
        const sum = lastPrice + total
        return sum
      } else {
        return 0
      }
    }
    const sum = cart?.reduce(reducer, 0)
    setValueProduct({ total: sum, subTotal: sum })
  }

  const handleSumTotal = (e: React.ChangeEvent<HTMLInputElement>) => {
    const discount = parseFloat(e.target.value)
    const total = valueProduct.subTotal - (valueProduct.subTotal * discount) / 100
    setValueProduct({ ...valueProduct, discount: discount, total: total })
  }

  return (
    <>
      <Head>
        <title>Checklist</title>
      </Head>
      <Box p="2em">
        <Box>
          {cart && cart.length > 0 ? <h3>List items</h3> : <h3>No items selected</h3>}
          {cart &&
            cart.map((item: ProductsType, i: number) => (
              <Box key={i}>
                <Card
                  item={item}
                  handleAddQuantity={() => handleAddQuantity(item)}
                  handleSubtractQuantity={() => handleSubtractQuantity(item)}
                  handleRemoveFromCart={() => handleRemoveFromCart(item, i)}
                />
              </Box>
            ))}
        </Box>
        {cart && cart.length > 0 && (
          <Box mt="1em">
            {cart && <Coupon valueProduct={valueProduct} />}
            <Box mt="1em" w="100%">
              <Button width="100%" type="primary" label="Confirm" onClick={() => router.push('/checkout')} />
            </Box>
          </Box>
        )}
      </Box>
    </>
  )
}

export default Checkout
