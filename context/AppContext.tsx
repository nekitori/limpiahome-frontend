import * as React from 'react'

import useInitialState from '../hooks/useInitialState'
import { UseInitializeReturn } from '../types/app'

const AppContext = React.createContext({})

export const AppContextProvider = ({ children }) => {
  const initialStorage = useInitialState()

  return <AppContext.Provider value={initialStorage}>{children}</AppContext.Provider>
}

export const useAppContext = () => {
  const dataApp = React.useContext<UseInitializeReturn>(AppContext)

  if (dataApp === undefined) {
    throw new Error('Debes estar dentro del context global')
  }

  return dataApp
}
