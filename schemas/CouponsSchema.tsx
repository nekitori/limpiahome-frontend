import * as Yup from 'yup'
const CouponsSchema = Yup.object().shape({
  code: Yup.string().required('Ingrese un código').min(6, 'Ingrese 6 caracteres como maximo.'),
  discount: Yup.string().required('Seleccione un descuento')
})

export default CouponsSchema
