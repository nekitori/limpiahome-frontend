import * as Yup from 'yup'

const ProductSchema = Yup.object().shape({
  name: Yup.string().required(),
  description: Yup.string().required(),
  price: Yup.number().min(0).required(),
  presentation: Yup.string().required(),
  image: Yup.object().required()
})

export default ProductSchema
