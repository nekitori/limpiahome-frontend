import * as Yup from 'yup'

const SignUpSchema = Yup.object().shape({
  email: Yup.string().email('Ingresa un correo valido').required('Ingrese un correo electrónico.'),
  password: Yup.string().required('Ingrese una contraseña.').min(6, 'Ingrese 6 caracteres como minimo.'),
  passwordConfirmation: Yup.string()
    .required('Ingrese una contraseña.')
    .oneOf([Yup.ref('password'), null], 'No coincide las contraseñas')
})

export default SignUpSchema
