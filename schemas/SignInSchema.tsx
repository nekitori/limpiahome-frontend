import * as Yup from 'yup'

const SignInSchema = Yup.object().shape({
  email: Yup.string().email('Ingresa un correo valido').required('Ingrese un correo electrónico.'),
  password: Yup.string().required('Ingrese una contraseña.').min(6, 'Ingrese 6 caracteres como minimo.')
})

export default SignInSchema
