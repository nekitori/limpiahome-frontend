module.exports = {
  login: 'Iniciar sesión',
  register: 'Registrate',
  email: 'Correo electronico',
  password: 'Contraseña',
  passwordConfirm: 'Confirmar contraseña'
}
