module.exports = {
  h1: 'Hola, bienvenido a Limpia Home.',
  p1:
    'Somos una empresa creada con el objetivo de reducir el uso de plásticos de un solo uso que afectan de manera importante a nuestro planeta.',
  p2:
    'Sabemos que los productos de cuidado personal, del hogar y para mantener nuestra ropa limpia son indispensables, sin embargo, suelen venir en envases de plástico que se usan y se desechan, además de que ocupan mucho espacio en los tiraderos.',
  p3: `En Limpia Home queremos que puedas re utilizar esos envases por mucho tiempo a través del rellenado.
 Nuestros productos vienen envasados en bolsas 100% biodegradables, de esta forma podrás seguir usando las botellas de plástico que ya tienes en casa.`,
  p4: `Entra a limpiahome.com, añade al carrito los artículos que necesitas, finaliza tu compra y recibelos en la puerta de tu casa.`,
  p5: `¿Quieres más? Entonces suscribete, de esta forma recibirás tu kit de forma periódica (tu decides la frecuencia), además no pagarás gastos de envío y tendrás en automático un 20% de descuento sobre el valor total de tu kit.`,
  p6: `¿Tienes alguna duda? Escríbenos por medio de nuestro chat, redes sociales o WhatsApp. Te atendemos todos los días de 8 am a 8 pm.`
}
