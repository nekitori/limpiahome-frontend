module.exports = {
  home: {
    welcome: 'Bienvenido a Limpiahome',
    price: 'precio'
  },
  notFound: {
    message: '¿Qué estás haciendo aquí? Esta página no existe como el amor de ella.',
    back: 'Ir al inicio'
  }
}
