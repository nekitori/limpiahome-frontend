module.exports = {
  h1: 'Hello, welcome to Limpia Home',
  p1:
    'We are a company created with the aim of reducing the use of single-use plastics that significantly affect our planet.',
  p2: `We know that personal care products, household products and to keep our clothes clean are essential, however, they usually come in plastic containers that are used and discarded, in addition to taking up a lot of space in landfills.`,
  p3: `At Limpia Home we want you to be able to reuse these containers for a long time through refilling.  `,
  p4: `
  Go to limpiahome.com, add the items you need to the cart, complete your purchase and receive them at the door of your house.`,
  p5: `Do you want more? Then subscribe, in this way you will receive your kit periodically (you decide the frequency), also you will not pay shipping costs and you will automatically have a 20% discount on the total value of your kit.`,
  p6: `
  Do you have any doubt? Write us through our chat, social networks or WhatsApp. We serve you every day from 8 am to 8 pm`
}
