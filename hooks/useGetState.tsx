import { useContext } from 'react'
import { useAppContext } from 'hooks/useAppContext'
import { AppState } from 'types/app'

export const useGetState = (property: string): AppState => {
  const { state } = useAppContext()

  if (property) {
    if (state.hasOwnProperty(property)) {
      return state[property]
    } else {
      throw new Error(`property "${property}" doesn't exist on state`)
    }
  }

  return state
}
