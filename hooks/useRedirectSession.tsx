import { useRouter } from 'next/router'

import { USER_SESSION, CLIENT, ADMIN } from 'constans'

interface UseRedirectSession {
  redirectSession(): void
  redirectToLogon(): void
  redirectAdmin(): void
}

const useRedirectSession = (): UseRedirectSession => {
  const router = useRouter()

  const redirectSession = () => {
    const user_session = JSON.parse(localStorage.getItem(USER_SESSION))

    if (user_session) {
      if (user_session.roleName === CLIENT) {
        router.push('/products')
      } else router.push('/admin')
    }
  }

  const redirectToLogon = () => {
    const user_session = JSON.parse(localStorage.getItem(USER_SESSION))

    if (!user_session) {
      router.push('/logon')
    }
  }

  const redirectAdmin = () => {
    const user_session = JSON.parse(localStorage.getItem(USER_SESSION))

    if (user_session) {
      if (user_session.roleName !== ADMIN) {
        router.push('/products')
      }
    } else {
      router.push('/logon')
    }
  }

  return { redirectSession, redirectToLogon, redirectAdmin }
}

export default useRedirectSession
