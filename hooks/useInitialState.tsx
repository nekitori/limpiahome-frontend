import { useState, useEffect } from 'react'
import initialState from '../initialState'
import cartActions from 'actions/cart'
import couponActions from 'actions/coupon'
import fetchActions from 'actions/fetch'

const useInitialState = () => {
  const [state, setState] = useState(initialState)

  return {
    ...cartActions(state, setState),
    ...couponActions(state, setState),
    ...fetchActions(state, setState),
    state
  }
}

export default useInitialState
