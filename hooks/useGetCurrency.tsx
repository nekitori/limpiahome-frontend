import { useRouter } from 'next/router'

export const useGetCurrency = () => {
  const { locale } = useRouter()

  switch (locale) {
    case 'es-CO':
      return 'COP'
    case 'es-MX':
      return 'MXN'
    case 'en-US':
      return 'USD'
  }
}
