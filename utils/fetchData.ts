import axios, { AxiosResponse, Method } from 'axios'

export async function fetchData(
  path = '',
  method: Method = 'GET',
  data = {},
  token = ''
): Promise<AxiosResponse | undefined> {
  const url = `${process.env.NEXT_PUBLIC_BASE_URL}${path}`

  try {
    const response = await axios({
      url,
      method,
      data,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })

    return response
  } catch (error) {
    return error.response
  }
}
