import { FaPen, FaTrashAlt } from 'react-icons/fa'
import styled from 'styled-components'

export const TrashIcon = styled(FaTrashAlt)`
  width: 32px;
  height: 32px;
  color: white;
  cursor: pointer;
  padding: 7px;
  background: #1b7bbf;
  border-radius: 50%;
`
export const PenIcon = styled(FaPen)`
  width: 20px;
  height: 32px;
  color: #55bdaa;
  cursor: pointer;
  padding-bottom: 15px;
`
