export const fonts = {
  mainfamily: `'Red Rose', 'cursive'`,
  secondfamily: `'Raleway', 'sans-serif'`,
  one: '42px',
  two: '36px',
  three: '30px',
  four: '24px',
  five: '18px',
  six: '16px',
  normal: '14px',
  small: '12px'
}

export const colors = {
  primary: {
    100: '#1B7BBF',
    200: '#0D578C',
    300: '#033259',
    400: '#001A33',
    500: '#53B2ED',
    600: '#7EC6F2',
    700: '#B6E3FA',
    800: '#E5F6FF',
    900: '#2D97E3',
    1000: '#1A202C'
  },
  secondary: {
    100: '#29CC29',
    200: '#51D94C',
    300: '#82E57E',
    400: '#BCF7BA',
    500: '#E6FFE5'
  },
  blancos: {
    100: '#FFFFFF',
    200: '#FAFAFA',
    300: '#F5F5F5',
    400: '#F0F0F0',
    500: '#EBEBEB'
  },
  gris: {
    100: '#D0D9D8',
    200: '#DFE5E5',
    300: '#EBF2F1',
    400: '#F5FCFB',
    500: '#9DA6A5'
  },
  black: {
    100: '#78807F',
    200: '#5A6160',
    300: '#3B403F',
    400: '#222625'
  }
}

export const bg = { light: 'blancos.500', dark: 'primary.300' }
export const bgHeader = { light: 'blancos.100', dark: 'primary.300' }
export const bgInput = { light: 'blancos.100', dark: 'primary.300' }
export const bgBtn = 'secondary.300'
export const bgModal = { light: 'blancos.100', dark: 'primary.1000' }
export const bgModal2 = { light: 'gris.300', dark: 'black.200' }
export const bgBtnSecondary = 'primary.100'
export const colorInput = { light: 'black.300', dark: 'blancos.300' }
export const cPlaceHolder = { light: 'gris.500', dark: 'blancos.200' }
