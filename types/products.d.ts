export type Rate = 1 | 2 | 3 | 4 | 5

export type ID = number

export interface CategoryType {
  id: ID
  name: string
}

export interface ProductsType {
  id: ID
  name: string
  description: string
  price: number
  presentation: string
  categories: ID[]
  image: string
  rate: Rate
  comments?: ID[]
  quantity?: number
}

export interface CommentType {
  id: ID
  userId: ID
  content: string
  date: Date
  rate: Rate
}
