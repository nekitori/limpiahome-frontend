import { CategoryType, ProductsType } from './products'

export interface ObjectOrder {
  id?: string
  stateOrder: 'buyed' | 'process' | 'delivered'
  buyer?: ObjectBuyer | null
  products?: ObjectItem[]
  // payment: OnCaptureData
}
interface Currency {
  mxn?: number
  usd?: number
}
export interface Coupon {
  id?: string
  code?: string
  descount?: string
}
export interface ObjectItem {
  quantity?: number
  currency?: string
  id?: string
  category?: string
  variant?: string
  refill?: string
  src?: string
  name?: string
  price?: Currency
  description?: string
}
export interface ObjectBuyer {
  name: string | File | null
  email: string | File | null
  address: string | File | null
  apt: string | File | null
  country: string | File | null
  state: string | File | null
  pc: string | File | null
  city: string | File | null
  phone: string | File | null
  rol: 'user' | 'admin'
}
export interface AppState {
  currency: 'usd' | 'mxn'
  cart: ProductsType[]
  buyer: ObjectBuyer | null
  orders: ObjectOrder[] | []
  products: ProductsType[]
  categories: CategoryType[]
  coupons: Coupon[]
}
export interface UseInitializeReturn {
  state?: AppState
  addToCart?: (payload: ProductsType) => void
  subtractQuantity?: (payload: ProductsType) => void
  addQuantity?: (payload: ProductsType) => void
  removeFromCart?: (item: ProductsType, i: number) => void
  addToBuyer?: (payload: ObjectBuyer) => void
  addNewOrder?: (payload: ObjectOrder) => void
  changeCurrency?: (payload: 'usd' | 'mxn') => void
  addCoupon: (newCoupon: string, descount: string) => void
  deleteCoupon: (idCoupon: string) => void
  changeStateOrder: (newState: 'buyed' | 'process' | 'delivered', idOrder: string) => void
}
export interface InputProps {
  labelName?: string
  icon?: React.ReactNode
  search?: boolean
  placeholder?: string
  select?: boolean
  seePassword?: boolean
  field?: { name: string; identifier: string }
  dataSelect?: { value: string; text: string }[]
}

export interface TableHeaderProps {
  labels: string[]
}

export interface ProductsFormProps {
  editId?: number
  defaultData?: {
    title: string
    description: string
    price: number
    image: string
  }
}

export interface TableRowProps {
  items: Array
  onEdit: (id: number) => void
  onDelete: (id: number) => void
}

export interface EditModalProps {
  isOpen?: boolean
  setIsOpen?: (state: boolean) => void
  onSave?: () => void
}

export interface PriceProps {
  children: {
    usd: number
    mxn: number
  }
}

export interface LogonValues {
  email: string
  password: string
  passwordConfirmation?: string
}

export interface ButtonProps {
  signup?: boolean
  login?: boolean
  facebook?: boolean
  google?: boolean
  onClick?: () => void
  label: string
}

export interface LoginProps {
  handleSubmit: () => void
}

export interface RegisterProps {
  handleSubmit: () => void
}

export interface CardProp {
  item: ProductsType
  handleRemoveFromCart: () => void
  handleSubtractQuantity: () => void
  handleAddQuantity: () => void
}
