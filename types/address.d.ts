export interface AddressProps {
  id: number
  billing?: boolean
  country?: string
  city?: string
  zipCode?: string
  street?: string
  externalNumber?: string
  fullName?: string
  phone?: string
}
