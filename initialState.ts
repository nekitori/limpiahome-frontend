import { AppState } from './types/app'

const initialState: AppState = {
  currency: 'mxn',
  cart: [
    {
      id: 0,
      categories: [0],
      name: 'Detergente Liquido',
      presentation: 'Ropa Color',
      description: `Colores intensos es un detergente líquido específicamente diseñado para tu ropa de color.
      • Actúa sobre tu ropa reviviendo los tonos y manteniendo la forma de tus prendas
      • Renueva la intensidad del color de tu ropa mientras cuida las fibras
      • Mantiene tu ropa bonita y brillante
      • Mantiene los colores vivos hasta 5 veces más`,
      price: 120,
      image: 'https://arepa.s3.amazonaws.com/camiseta.png',
      rate: 4,
      comments: [0, 1]
    }
  ],
  buyer: {
    name: null,
    email: null,
    address: null,
    apt: null,
    country: null,
    state: null,
    pc: null,
    city: null,
    phone: null,
    rol: 'admin'
  },
  orders: [
    {
      id: '1',
      stateOrder: 'buyed',
      buyer: {
        name: 'sujeto-prueba',
        email: 'sujeto@gmail.com',
        address: 'direccion de sujeto',
        apt: null,
        country: 'Rusia',
        state: null,
        pc: null,
        city: 'mosku',
        phone: '123456789',
        rol: 'admin'
      },
      products: [
        {
          id: '1',
          currency: 'mxn',
          category: 'cocina',
          variant: '',
          refill: '',
          src: '',
          name: '',
          // price: '',
          description: ''
        }
      ]
    },
    {
      id: '2',
      stateOrder: 'buyed',
      buyer: {
        name: 'sujeto-prueba',
        email: 'sujeto@gmail.com',
        address: 'direccion de sujeto',
        apt: null,
        country: 'Rusia',
        state: null,
        pc: null,
        city: 'mosku',
        phone: '123456789',
        rol: 'admin'
      },
      products: [
        {
          id: '1',
          currency: 'mxn',
          category: 'cocina',
          variant: '',
          refill: '',
          src: '',
          name: '',
          // price: '',
          description: ''
        }
      ]
    }
  ],
  categories: [],
  coupons: [
    {
      id: '1',
      code: 'code1 con texto mas largo para ver los puntos suspensivos',
      descount: '20'
    },
    {
      id: '2',
      code: 'code2',
      descount: '10'
    },
    {
      id: '3',
      code: 'code3',
      descount: '20'
    },
    {
      id: '4',
      code: 'code4 con texto mas largo para ver los puntos suspensivos',
      descount: '10'
    },
    {
      id: '5',
      code: 'code5',
      descount: '20'
    },
    {
      id: '6',
      code: 'code6',
      descount: '10'
    },
    {
      id: '7',
      code: 'code7',
      descount: '20'
    }
  ],
  products: []
}

export default initialState
