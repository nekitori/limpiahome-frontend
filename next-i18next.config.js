module.exports = {
  i18n: {
    localeExtension: 'js',
    defaultLocale: 'es-MX',
    locales: ['es-MX', 'es-CO', 'en-US']
  }
}
