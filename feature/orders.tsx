import Head from 'next/head'
import { useLayoutEffect } from 'react'

import { useAppContext } from 'hooks/useAppContext'
import { ObjectOrder } from 'types/app'
import useRedirectSession from 'hooks/useRedirectSession'

const ViewStateOrder: React.FC = () => {
  const {
    state: {
      buyer: { rol },
      orders
    },

    changeStateOrder
  } = useAppContext()

  const { redirectToLogon } = useRedirectSession()

  useLayoutEffect(() => {
    redirectToLogon()
  }, [])

  const onChange = (idOrder: string) => (e: React.FormEvent<HTMLSelectElement>) => {
    const {
      target: { value }
    } = e
    changeStateOrder(value, idOrder)
  }

  const renderOrdersUser = () =>
    orders.map((order: ObjectOrder) => (
      <li key={order.id}>
        <h2>{`orden with id: ${order.id}`}</h2>
        <h2>{`state of order: ${order.stateOrder}`}</h2>
      </li>
    ))

  const renderOrdersAdmin = () =>
    orders.map((order: ObjectOrder) => (
      <li key={order.id}>
        <h2>{`orden with id: ${order.id}`}</h2>
        <h2>{`state of order: ${order.stateOrder}`}</h2>
        <select name="cars" id="cars" onChange={onChange(order.id)}>
          <optgroup>
            <option value="buyed">Buyed</option>
            <option value="process">Process</option>
            <option value="delivered">Delivered</option>
          </optgroup>
        </select>
      </li>
    ))

  return (
    <div>
      {rol === 'user' ? (
        <>
          <h1>Eres un: USUARIO</h1>
          <ul>{renderOrdersUser()}</ul>
        </>
      ) : (
        <>
          <h1>Eres un: ADMINISTRADOR</h1>
          <ul>{renderOrdersAdmin()}</ul>
        </>
      )}
    </div>
  )
}

export default ViewStateOrder
