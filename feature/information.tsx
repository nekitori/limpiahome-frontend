import Head from 'next/head'
import ActiveLink from '@components/Atoms/ActiveLink/ActiveLink'
import { useAppContext } from 'hooks/useAppContext'
import React, { useRef, useLayoutEffect } from 'react'
import { ObjectBuyer, ObjectItem } from '../types/app'
import useRedirectSession from 'hooks/useRedirectSession'

const Information: React.FunctionComponent = () => {
  const { state, addToBuyer } = useAppContext()
  const cart = state?.cart

  const { redirectToLogon } = useRedirectSession()

  useLayoutEffect(() => {
    redirectToLogon()
  }, [])

  const form = useRef<HTMLFormElement>(null)

  const handleSubmit = () => {
    if (form && form.current) {
      const formData = new FormData(form.current)
      const buyer: ObjectBuyer = {
        name: formData.get('nm'),
        email: formData.get('ml'),
        address: formData.get('ddrss'),
        apt: formData.get('prtmnt'),
        country: formData.get('cntr'),
        state: formData.get('stt'),
        pc: formData.get('pstlcd'),
        city: formData.get('ct'),
        phone: formData.get('phn')
      }
      if (addToBuyer) {
        addToBuyer(buyer)
      }
    }
  }
  return (
    <>
      <Head>
        <title>Information</title>
      </Head>
      <div>
        <div>
          <h3>Contact Form:</h3>
        </div>
        <div>
          <form ref={form}>
            <input type="text" placeholder="Full name" name="nm" />
            <input type="text" placeholder="E-mail" name="ml" />
            <input type="text" placeholder="Address" name="ddrss" />
            <input type="text" placeholder="Apto" name="prtmnt" />
            <input type="text" placeholder="Country" name="cntr" />
            <input type="text" placeholder="State" name="stt" />
            <input type="text" placeholder="Postal Code" name="pstlcd" />
            <input type="text" placeholder="City" name="ct" />
            <input type="text" placeholder="Phone" name="phn" />
          </form>
          <div>
            <div>
              <ActiveLink href="/checklist" label="Go back" />
            </div>
            <button aria-label="continue" type="button" onClick={handleSubmit}>
              Continue
            </button>
          </div>
        </div>
      </div>
      <div>
        <h3>List of items:</h3>
        {cart &&
          cart.map((_item: ObjectItem) => {
            ;<div>
              <div>
                <h4>{_item.title}</h4>
                <span>{_item.price?.mxn}</span>
              </div>
            </div>
          })}
      </div>
    </>
  )
}

export default Information
