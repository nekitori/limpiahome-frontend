import Head from 'next/head'
import { Text, useColorModeValue, Box } from '@chakra-ui/react'
import Carousel from '../../components/Products/Carousel'
import CommentBox from '../../components/Products/CommentBox'
import { bgModal2 } from '../../styles/variables'
import Button from '../../components/Shared/Button'
import { fetchData } from 'utils/fetchData'

function Details({ products = {} }): JSX.Element {
  const bg = useColorModeValue(bgModal2.light, bgModal2.dark)

  return (
    <>
      <Head>
        <title>{products.name} Details</title>
      </Head>
      <Box w="90%" m="0 auto">
        <Carousel />
        <Text bg={bg} p="1rem">
          {products.description}
        </Text>
        <CommentBox />
        <Box bg={bg} p="1rem" mt="0.3em" textAlign="center">
          <Text mb="0.5em">¿Dudas de si este es el producto ideal para esa mancha? Consulta con nuestros expertos</Text>
          <br />
          <Button label="FAQ" type="secondary" />
        </Box>
      </Box>
    </>
  )
}

export async function getStaticProps({ locale, params }) {
  const response = await fetchData(`products/${params.id}`)

  let products
  if (locale === 'en-US') {
    products = response.data.data.lang[0]
  } else {
    products = response.data.data.lang[1]
  }

  return {
    props: {
      products
    }
  }
}

// export async function getStaticPaths({ locales }) {
//   const response = await fetchData(`products`)
//   const products = response.data.data

//   let paths = products.map(product => ({ params: { id: product.id.toString() }, locale: 'en-US' }))
//   paths = [...paths, ...products.map(product => ({ params: { id: product.id.toString() }, locale: 'es-MX' }))]
//   paths = [...paths, ...products.map(product => ({ params: { id: product.id.toString() }, locale: 'es-CO' }))]

//   return {
//     paths,
//     fallback: false
//   }
// }

export default Details
