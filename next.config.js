/* eslint-disable @typescript-eslint/no-var-requires */
// Information about this file here:
// https://nextjs.org/docs/api-reference/next.config.js/introduction
const { i18n } = require('./next-i18next.config')
const withPWA = require('next-pwa')

module.exports = withPWA({
  baseUrl: '.',
  esModule: true,
  i18n,
  pwa: {
    dest: 'public',
    disable: process.env.NODE_ENV === 'development'
  },
  typescript: {
    // !! WARN !!
    // Dangerously allow production builds to successfully complete even if
    // your project has type errors.
    // !! WARN !!
    ignoreBuildErrors: true
  }
})
