export default (state, setState) => {
  const addCategoriesAndProducts = (categories: any[], products: any[]) => {
    setState({ ...state, products: products, categories: categories })
  }

  return {
    addCategoriesAndProducts
  }
}
