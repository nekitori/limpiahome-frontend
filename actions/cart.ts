import { ProductsType } from '../types/products'
import { ObjectBuyer, ObjectOrder } from '../types/app'

export default function cartActions(state, setState) {
  const subtractQuantity = (_payload: ProductsType) => {
    setState({
      ...state,
      cart: state.cart.map(item => (item.id === _payload.id ? { ...item, quantity: item.quantity - 1 } : item))
    })
  }

  const addQuantity = (_payload: ProductsType) => {
    setState({
      ...state,
      cart: state.cart.map(item => (item.id === _payload.id ? { ...item, quantity: item.quantity + 1 } : item))
    })
  }

  const addToCart = (payload: ProductsType) => {
    // const productCart = state?.cart?.filter(product => product.id === payload.id)

    // if (productCart != '') {
    //   setState({
    //     ...state,
    //     cart: state.cart.map(item => (item.id === payload.id ? { ...item, quantity: item.quantity + 1 } : item))
    //   })
    // } else {
    //   setState({
    //     ...state,
    //     cart: [...state.cart, { ...payload, quantity: 1 }]
    //   })
    // }

    setState({
      ...state,
      cart: [...state.cart, { ...payload, quantity: 1 }]
    })
  }

  const addToBuyer = (payload: ObjectBuyer) => {
    setState({
      ...state,
      buyer: payload
    })
  }
  const addNewOrder = (payload: ObjectOrder) => {
    setState({
      ...state,
      orders: [...state.orders, payload]
    })
  }

  const removeFromCart = (_payload: ProductsType, indexToRemove: number) => {
    setState({
      ...state,
      cart: state?.cart?.filter((_item, indexCurrent) => indexCurrent !== indexToRemove)
    })
  }

  const changeStateOrder = (newState: 'buyed' | 'process' | 'delivered', idOrder: string) => {
    setState({
      ...state,
      orders: state.orders.map(order => {
        if (order.id === idOrder) {
          return {
            ...order,
            stateOrder: newState
          }
        } else {
          return {
            ...order
          }
        }
      })
    })
  }

  return {
    addQuantity,
    subtractQuantity,
    addToCart,
    removeFromCart,
    addNewOrder,
    addToBuyer,
    changeStateOrder
  }
}
