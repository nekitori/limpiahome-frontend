export default (state, setState) => {
  const addCoupon = (newCoupon: string, descount: string) => {
    const _newCoupon = {
      id: `${newCoupon}+${descount}`,
      code: newCoupon,
      descount
    }

    console.log({ _newCoupon })

    setState({
      ...state,
      coupons: [...state.coupons, _newCoupon]
    })
  }

  const deleteCoupon = (idCoupon: string) => {
    setState({
      ...state,
      coupons: state.coupons.filter(coupon => coupon.id !== idCoupon)
    })
  }

  return {
    addCoupon,
    deleteCoupon
  }
}
